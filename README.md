
![image](assets/EBS.png)

---
# Enterprise Breeding System (EBS) CI/CD Pipeline

> This repository contains all the necessary resources to deploy EBS software components either as part of a CI/CD pipeline or for a full-fledged deployment. It heavily relies on Docker and Docker Swarm technologies in order
for the services to interact together.

## Table Of Contents
* [General info](#markdown-header-general-info)
    * [Directory Structure](#markdown-header-directory-structure)
* [Getting Started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
* [Contact](#markdown-header-contact)

## General info

### Directory Structure

	|── archived                 			# Archived files
	|── assets                   			# Resources such as logos, icons, etc
	|── components               			# Docker stack files for deploying the different EBS software components
	    |── af								# Resources for deploying services related to the EBS Analytics Framework
		|── breeding_analytics        		# Resources for deploying services related to Breeding Analytics
		|── breeding_station_operations		# Resources for deploying services related to Breeding Station Operations
		|── core_breeding        			# Resources for deploying services related to Core Breeding
		|── coruscant						# Resources for deploying services related to Traefik
		|── gobii_gdm						# Resources for deploying services related to GOBii GDM
		|── service_management				# Service Management stack
		|── tenant               			# Resources for deploying services related to Tenant Portal
	|── config								# Contains configuration files for managing the EBS Docker Swarm
    |__ terraform                			# !needs own repo! Terraform scripts for provisioning hosts to deploy EBS environments
    |__ utility                  			# Utility scripts for supporting CI/CD processes as well as to support DevOps tasks in the EBS level

## Getting Started

### Prerequisites

#### Docker Swarm Cluster
⚠️ 1. A pre-initialized docker swarm cluster

#### User Account and Access
1. Docker Hub account (with access to images)
2. BitBucket account (with access to to all repositories indicated in this guide)

#### Hardware Requirements:
1. 100 GB HDD
2. 4 GB RAM
3. 2 CPU Cores

#### Software Requirements

1. Ubuntu 18.04 LTS or CentOS 7 (these are recommended over Windows because of configuration and compatibility issues that tend to come up more often in Windows)
2. Git
3. Docker Engine CE (v19.03+)

### Checking out and deploying

[1] Clone the EBS CI/CD Pipeline repository and change your current directory to `deployment-tool`:

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool
```

[2] Update _config/ebs_system_config_ to change following:

- Docker stack and network name
```bash
## Default values
EBS_DOCKER_STACK_NAME=ebs
EBS_DOCKER_NETWORK_NAME=ebs_network
```

- Vault credentials
> ⚠️ For local deployment, you may skip this step. ** _Contact your administrator for the Vault credentials_
```bash
VAULT_CLIENT_TOKEN=xxxx
HASHICORP_VAULT_URL=https://vault.ebsproject.org
```

[3] Go to the _components_ folder and deploy the _coruscant_ stack.

```bash
cd components/coruscant

bash deploy_stack.sh
```

[4] Choose the service you want to deploy next.

_*** Detailed instructions are provided for each component._

Ex:
```bash
cd ../af
```

## Contact

Report an issue [here](https://ebsproject.atlassian.net/jira/servicedesk/projects/ESD).
