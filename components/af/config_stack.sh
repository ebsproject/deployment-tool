#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting the configuration of Analytics... $RESET"
echo;

#-----------------------------------------------------------------------------#
# Initializing Directories

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [1/1] Initializing directories.. $RESET"
echo;

set -x
mkdir -p af-volume/config
mkdir -p af-volume/ebs-af/ebs-af-volume/input
mkdir -p af-volume/ebs-af/ebs-af-volume/output
mkdir -p af-volume/ebs-af/ebs-af-volume/archive
mkdir -p af-volume/ebs-af/ebs-af-volume/scratch
mkdir -p af-volume/ebs-af/ebs-af-volume/ebs-af-analytics-db/postgres
mkdir -p af-volume/ebs-af/ebs-af-volume/ebs-af-db/postgres
set +x

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;
#-----------------------------------------------------------------------------#

echo "$GREEN ---------------------------------------------------- $RESET"
echo "$GREEN ☑ Configuration is complete. $RESET"