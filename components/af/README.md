
![image](../../assets/EBS.png)

---
# AF CI/CD Deployment 

This tool deploys Analytics Framework as a separate stack on an existing Docker Swarm for EBS.

-------------------------------------------------------------------------------

### Pre-Deployment Steps

#### Pre-Deployment Repos Setup

> ⚠️ The following steps apply to new installations only. For succeeding deployments, you may proceed immediately to the Deployment section.

[1] Clone the EBS Deployment Tool and change your current directory to `deployment-tool`:

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool/components/af/
```

[3] Update env file with version and variables for deployment.  

*** _These variables should be updated prior to deploy/config per branch & release._

[4] Configure the stack

```bash
bash config_stack.sh
```

[5] Ensure that dependent external services are deployed (i.e Traefik). Go to `components/coruscant` and deploy the stack.


-------------------------------------------------------------------------------


### **Deploy the Stack**

```bash
bash deploy_stack.sh
```

-------------------------------------------------------------------------------

### Post-Deployment Steps

[1] To mount the database for analytics, execute the command below:

```bash
bash init_analytics_db.sh
```

-------------------------------------------------------------------------------

#### ⚠️ **_To bring down the infrastructure, execute:_**

```bash
docker stack rm <EBS_DOCKER_STACK_NAME>
```