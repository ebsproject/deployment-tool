#!/bin/bash

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting the restoration of AFDB.. $RESET"
echo;

#-----------------------------------------------------------------------------#

export AF_DIR=`pwd`
export ANALYTICSDB=analytics
export ANALYTICS_DUMP_FILE=aeo_database_2020.07.21-analytics.pg.sql

## Restore the EBS Analytics Framework database:

## Check if the ebs-af-postgres docker service is running

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Checking if ebs-af-postgres is running...$RESET"

if [ -z ` docker ps -q -f name=ebs_ebs-af-postgres` ]; 
then
    echo;
    echo "$RED ☒ Failed to create services. Please re-run the script.$RESET"
    exit
else

    ## Check if analytics db is existing
    echo "$BLUE Checking if analytics db is existing...$RESET"

    if ! docker exec $(docker ps -q -f name=ebs_ebs-af-postgres) bash -c "psql -U postgres -c 'select 1' -d $ANALYTICSDB &>dev/null" ;
    then 
        echo "$BLUE Creating database...$RESET"

        docker exec $(docker ps -q -f name=ebs_ebs-af-postgres) bash -c "psql -U postgres -c 'create database $ANALYTICSDB'"
        
        docker exec $(docker ps -q -f name=ebs_ebs-af-postgres) bash -c "mkdir -p dump"

        docker cp scripts/db/${ANALYTICS_DUMP_FILE} $(docker ps -q -f name=ebs_ebs-af-postgres):/dump/${ANALYTICS_DUMP_FILE}

        docker exec $(docker ps -q -f name=ebs_ebs-af-postgres) bash -c "psql -U postgres -h localhost $ANALYTICSDB -f dump/${ANALYTICS_DUMP_FILE}"

        echo "$BLUE Completed database restore...$RESET"

        echo "$BLUE Allowing access to database...$RESET"

        docker exec $(docker ps -q -f name=ebs_ebs-af-postgres) bash -c 'echo "host all all 0.0.0.0/0 md5" >> /data/postgres/pg_hba.conf'

        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    else
        echo;
        echo "$YELLOW ⚠ Database already exists...$RESET"
        echo;
    fi
fi

#-----------------------------------------------------------------------------#

echo "$GREEN ---------------------------------------------------- $RESET"
echo "$GREEN ☑ Configuration is complete. $RESET"