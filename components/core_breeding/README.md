
![image](../../assets/EBS.png)

---
# Core Breeding CI/CD Deployment 

This tool deploys Core Breeding as a separate stack on an existing Docker Swarm for EBS.

-------------------------------------------------------------------------------

### Pre-Deployment Steps

#### Pre-Deployment Repos Setup

> ⚠️ The following steps apply to new installations only. For succeeding deployments, you may proceed immediately to the Deployment section.

[1] Clone the EBS Deployment Tool and change your current directory to `deployment-tool`:

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool/components/core_breeding/
```

[2] Update the following files based on the environment to be used in deployment (i.e localhost)

*** _These variables should be updated prior to deploy/config per branch & release._

_env_

** _Contact your administrator for the Service Gateway credentials_
```bash
## Default
## Load fixture data
LIQUIBASE_CONTEXTS=schema,template,fixture
LIQUIBASE_LABELS=clean,develop

## Load template data only
LIQUIBASE_CONTEXTS=schema,template
LIQUIBASE_LABELS=clean

## Service Gateway Configuration
CBAPI_AUTH_URL=https://sg.ebsproject.org:9443/oauth2/authorize
CBAPI_CLIENT_ID=xxx
CBAPI_CLIENT_SECRET=xxx
CBAPI_REDIRECT_URI=https://cbapi.local/v3/auth/callback
CBAPI_TOKEN_URL=https://sg.ebsproject.org:9443/oauth2/token
CBAPI_JWKS_URI=https://sg.ebsproject.org:9443/oauth2/jwks
CBAPI_JWT_ALGORITHM=RS256

### DEPLOYMENT PARAMETERS ###
CB_HOST=cb.local
CBAPI_HOST=cbapi.local
```

_cb-volume/config/cbconfig_

** _Contact your administrator for the Service Gateway credentials_
```bash
## Service Gateway Configuration
CB_SG_AUTH_URL=https://sg.ebsproject.org:9443/oauth2/authorize
CB_SG_CLIENT_ID=xxx
CB_SG_CLIENT_SECRET=xxx
CB_SG_REDIRECT_URI=https://cb.local/index.php/auth/default/verify
CB_SG_TOKEN_URL=https://sg.ebsproject.org:9443/oauth2/token

## DOMAIN LINKS

CB_URL=https://cb.local/
```

[3] Ensure that dependent external services are deployed (i.e Traefik). Go to `components/coruscant` and deploy the stack.

-------------------------------------------------------------------------------


### **Deploy the Stack**

```bash
bash deploy_stack.sh
```

_To use the database credentials from vault_
```bash
bash deploy_stack.sh <secret path>

## Sample:
bash deploy_stack.sh ebs-dev-db/creds/readwrite-cb
```

#### **Data Loading**

_For first-time deployment, loading of data may take a long time._

- Check if loading of data is finished by running:
```bash
docker service logs --raw ebs_cb-db
```

- Wait until the logs show the following:
```
Liquibase: Update has been successful.
<DB name> initialized successfully
```

- To check the progress, run these commands in order:
```bash
docker ps
docker exec -it <container ID of ebs_cb-db> bash
psql -U postgres -h localhost <DB name>
select id, orderexecuted, contexts, labels from databasechangelog order by orderexecuted desc;
```

-------------------------------------------------------------------------------

#### ⚠️ **_To bring down the infrastructure, execute:_**

```bash
docker stack rm <EBS_DOCKER_STACK_NAME>
```