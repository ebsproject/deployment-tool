
![image](../../assets/EBS.png)

---
# Coruscant Deployment

This tool deploys Coruscant as a separate stack on an existing Docker Swarm for EBS.

-------------------------------------------------------------------------------

### Pre-Deployment Steps

#### Pre-Deployment Repos Setup

> ⚠️ The following steps apply to new installations only. For succeeding deployments, you may proceed immediately to the Deployment section.

[1] Clone the EBS Deployment Tool and change your current directory to `deployment-tool`:

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool/components/coruscant/
```

[2] Update the following files based on the environment to be used in deployment (i.e localhost)

*** _These variables should be updated prior to deploy/config per branch & release._

_env_
```bash
RABBITMQ_HOST=xxxx
MATOMO_HOST=matomo.local
FILE_API_HOST=fileapi.local
```

-------------------------------------------------------------------------------

### **Deploy the Stack**

```bash
bash deploy_stack.sh
```

-------------------------------------------------------------------------------

#### ⚠️ **_To bring down the infrastructure, execute:_**

```bash
docker stack rm <EBS_DOCKER_STACK_NAME>
```

-------------------------------------------------------------------------------

> ⚠️ This is not required.If this step is not done, the assumption is that it will use the self-signed certificates that Traefik generates.
#### **Applying SSL certificates in Traefik**

> The following steps provide instructions on how to apply the SSL certificates in Traefik. It is assumed in this guide that the SSL certificates are provided in a tar.gz file. See [this Confluence page](https://ebsproject.atlassian.net/wiki/spaces/DO/pages/125436245/Self-Signed+Certificates+for+Localhost) for more info on how to create Self-signed certificates for localhost.

[1] Go to the directory where the tar.gz file is and extract the file. Go to the folder created after extracting the file. Check if the pem files are in the folder.

```bash
tar –xvzf sample.tar.gz

cd sample
ls
```

[2] Copy the privkey.pem and fullchain.pem to the certificates directory for Traefik.

```bash
cp privkey.pem {EBS_DEPLOYMENT_TOOL}/components/coruscant/coruscant-volume/traefik/certificates/privkey.pem

cp fullchain.pem {EBS_DEPLOYMENT_TOOL}/components/coruscant/coruscant-volume/traefik/certificates/fullchain.pem
```

[3] If there is a running deployment of Traefik, remove the service first and deploy the coruscant to reflect the changes.

```bash
docker service rm ebs_traefik
cd {EBS_DEPLOYMENT_TOOL}/components/coruscant
bash deploy_stack.sh
```