
![image](../../assets/EBS.png)

---

# GOBii GDM

This tool deploys the GOBii GDM components as a separate stack on an existing Docker Swarm deployment for EBS.

---

## Pre-Deployment Steps

### Pre-Deployment Repos Setup

> ⚠️ The following steps apply to new installations only. For succeeding deployments, you may proceed immediately to the Deployment section.

[1] Clone the EBS Deployment Tool and check out the desired branch (if you have not already done so):

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool/components/gobii_gdm

```

[2] Update the following files based on the environment to be used in deployment (i.e localhost)

*** _These variables should be updated prior to deploy/config per branch & release._

`env`

```bash
GOBII_HOST=xxxx
GOBII_API_HOST=xxxx
```

[3] Ensure that dependent external services are deployed (i.e Traefik). Go to `components/coruscant` and deploy the stack.

---

## Deploy the Stack

```bash
bash deploy_stack.sh
```

### To use the database credentials from vault

```bash
bash deploy_stack.sh <secret path>

## Sample:
bash deploy_stack.sh ebs-dev-db/creds/readwrite-gdm
```

---

##  ️ To bring down the infrastructure, execute:

```bash
docker stack rm <EBS_DOCKER_STACK_NAME>
```
