#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting the deployment of Core System... $RESET"
echo;

#-----------------------------------------------------------------------------#

## Retrieve current directory
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [1/6] Retrieving current directory.. $RESET"
echo;

pwd=`pwd`
export CS_DATA_DIR=$pwd

## Create directories for Postgres
set -x
mkdir -p cs-volume/cs-db/postgres_etc
mkdir -p cs-volume/cs-db/postgres_lib
mkdir -p cs-volume/cs-db/postgres_log
set +x

export POSTGRES_ETC="$(cd cs-volume/cs-db/postgres_etc; pwd)"
export POSTGRES_LIB="$(cd cs-volume/cs-db/postgres_lib; pwd)"
export POSTGRES_LOG="$(cd cs-volume/cs-db/postgres_log; pwd)"

echo $CS_DATA_DIR
echo $POSTGRES_ETC
echo $POSTGRES_LOG
echo $POSTGRES_LIB

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for EBS network and stack
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [2/6] Loading EBS System variables.. $RESET"
echo;

set -a
set -x 
. ../../config/ebs_system_config
set +x
set +a

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;
#-----------------------------------------------------------------------------#

## Create Docker network for EBS
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [3/6] Creating EBS network.. $RESET"
echo;

{
    docker network create --ingress --driver overlay $EBS_DOCKER_NETWORK_NAME && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$YELLOW ⚠  EBS network already exists. $RESET"
    echo;
}

#-----------------------------------------------------------------------------#

## Pull Docker images
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [4/6] Loading Docker images.. $RESET"
echo;

{
    bash ../../utility/pull_images.sh env && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$RED ☒  Error in loading Docker images. $RESET"
    echo;
    exit 1
}

#-----------------------------------------------------------------------------#

## Generate values for db.env
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [5/6] Generating values for database credentials.. $RESET"
echo;

## Check if there is argument passed
## If yes, db.env will be updated
## Argument $1 is the value of secret path
if [ $# -eq 0 ]; then
    echo;
    echo "$YELLOW Skipping the step. $RESET"
    echo;
else
    {
        SECRET_PATH=$1 ## Sample: ebs-dev-db/creds/readwrite-cs
        echo $SECRET_PATH
        sed -i -e "s|xxxx|$SECRET_PATH|g" db.template.env.tmpl
        VAULT_TOKEN=$VAULT_CLIENT_TOKEN consul-template -vault-addr=$HASHICORP_VAULT_URL \
            -template="db.template.env.tmpl:db.env" -once && {
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
        }
    } || {
        echo;
        echo "$RED ☒  Error in generating values for database credentials. $RESET"
        echo;
        exit 1
    }
fi

#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for Core System stack and deploy

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [6/6] Deploying stack... $RESET"
echo;

{
    set -x
    env $(cat env db.env | grep ^[A-Z] | xargs) \
        docker stack deploy --with-registry-auth -c cs-stack.yml $EBS_DOCKER_STACK_NAME && {
            set +x
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
            echo "$GREEN ---------------------------------------------------- $RESET"
            echo "$GREEN ☑  Deployment is complete. $RESET"
            echo;
        }
} || {
    set +x
    echo;
    echo "$RED ☒  Error in deploying stack. Please run the script again. $RESET"
    echo;
}
