
![image](../../assets/EBS.png)

---
# Core System Deployment

This tool deploys Core System as a separate stack on an existing Docker Swarm for EBS.

-------------------------------------------------------------------------------

### Pre-Deployment Steps

#### Pre-Deployment Repos Setup

> ⚠️ The following steps apply to new installations only. For succeeding deployments, you may proceed immediately to the Deployment section.

[1] Clone the EBS Deployment Tool and change your current directory to `deployment-tool`:

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool/components/core_system/
```

[2] Update the following files based on the environment to be used in deployment (i.e localhost)

*** _These variables should be updated prior to deploy/config per branch & release._

_env_
```bash

CS_EBS_UI_PROXY_IMAGE_TAG=xxxx

CS_EBS_SHARED_DEPENDENCIES_IMAGE_TAG=xxxx

CS_EBS_LAYOUT_IMAGE_TAG=xxxx

EBS_CS_SG_USERNAME=xxxx
EBS_CS_SG_PASSWORD=xxxx

EBS_CS_API_CLIENT_ID=xxxx
EBS_CS_API_CLIENT_SECRET=xxxx

EBS_SG_USER=xxxx
EBS_SG_PASSWORD=xxxx
EBS_SG_CLIENT_ID=xxxx
EBS_SG_CLIENT_SECRET=xxxx

CS_PS_CLIENT_ID=xxxx
CS_PS_CLIENT_SECRET=xxxx

CB_API_SWARM_URL=xxxx

CS_PS_HOST=xxxx
CS_PS_CUPS_HOST=xxxx
CS_UI_HOST=xxxx
CS_API_HOST=xxxx

BSOAPI_URL=xxxx
CS_API_URL=xxxx
CUPS_URL=xxxx

LIB_VERSION=xxxx
```

[3] Ensure that dependent external services are deployed (i.e Traefik). Go to `components/coruscant` and deploy the stack.

-------------------------------------------------------------------------------

### **Deploy the Stack**

```bash
bash deploy_stack.sh
```

_To use the database credentials from vault_
```bash
bash deploy_stack.sh <secret path>

## Sample:
bash deploy_stack.sh ebs-dev-db/creds/readwrite-cs
```

-------------------------------------------------------------------------------

#### ⚠️ **_To bring down the infrastructure, execute:_**

```bash
## Can use either of the two below:

bash ../../utility/undeploy_services.sh <EBS_DOCKER_STACK_NAME> cs

docker stack rm <EBS_DOCKER_STACK_NAME>
```
