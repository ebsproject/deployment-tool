
![image](../../assets/EBS.png)

---
# Breeding Analytics #

This tool deploys the Breeding Analytics components as a separate stack on an existing Docker Swarm deployment for EBS.

-------------------------------------------------------------------------------

### Pre-Deployment Steps

#### Pre-Deployment Repos Setup

> ⚠️ The following steps apply to new installations only. For succeeding deployments, you may proceed immediately to the Deployment section.

[1] Clone the EBS Deployment Tool and check out the desired branch (if you have not already done so):

```bash
git clone https://{BITBUCKET_USERNAME}@bitbucket.org/ebsproject/deployment-tool.git

cd deployment-tool/components/breeding_analytics

```

[2] Update the following files based on the environment to be used in deployment (i.e localhost)

*** _These variables should be updated prior to deploy/config per branch & release._

_env_
```bash
BA_CLIENT_ID=xxxx
BA_CLIENT_SECRET=xxxx
BA_UI_COOKIE_PASSWORD=xxxx
ASREML_ACTIVATION_CODE=xxxx

BA_ARM_HOST=xxxx
BA_API_HOST=xxxx
BA_API2_HOST=xxxx
BA_PDM_API_HOST=xxxx
BA_UI_HOST=xxxx
```

[3] Ensure that dependent external services are deployed (i.e Traefik). Go to `components/coruscant` and deploy the stack.

-------------------------------------------------------------------------------


### **Deploy the Stack**

```bash
bash deploy_stack.sh
```

_To use the database credentials from vault_
```bash
bash deploy_stack.sh <secret path>

## Sample:
bash deploy_stack.sh ebs-dev-db/creds/readwrite-ba
```

-------------------------------------------------------------------------------

####  ️ **_To bring down the infrastructure, execute:_**

```bash
docker stack rm <EBS_DOCKER_STACK_NAME>
```
