#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting the deployment of Breeding Analytics... $RESET"
echo;

#-----------------------------------------------------------------------------#

## Retrieve current directory
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [1/6] Retrieving current directory.. $RESET"
echo;

pwd=`pwd`
export BA_DATA_VOLUME=$pwd
echo $BA_DATA_VOLUME

## Create directories for Postgres
set -x
mkdir -p ba-volume/ba-db/postgres_etc
mkdir -p ba-volume/ba-db/postgres_lib
mkdir -p ba-volume/ba-db/postgres_log

mkdir -p ba-volume/ba-db-v2/postgres_etc
mkdir -p ba-volume/ba-db-v2/postgres_lib
mkdir -p ba-volume/ba-db-v2/postgres_log
set +x

export POSTGRES_ETC="$(cd ba-volume/ba-db/postgres_etc; pwd)"
export POSTGRES_LIB="$(cd ba-volume/ba-db/postgres_lib; pwd)"
export POSTGRES_LOG="$(cd ba-volume/ba-db/postgres_log; pwd)"

export POSTGRES_ETC_V2="$(cd ba-volume/ba-db-v2/postgres_etc; pwd)"
export POSTGRES_LIB_V2="$(cd ba-volume/ba-db-v2/postgres_lib; pwd)"
export POSTGRES_LOG_V2="$(cd ba-volume/ba-db-v2/postgres_log; pwd)"

echo $POSTGRES_ETC
echo $POSTGRES_LOG
echo $POSTGRES_LIB

echo $POSTGRES_ETC_V2
echo $POSTGRES_LOG_V2
echo $POSTGRES_LIB_V2

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for EBS network and stack
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [2/6] Loading EBS System variables.. $RESET"
echo;

set -a
set -x 
. ../../config/ebs_system_config
set +x
set +a

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Create Docker network for EBS
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [3/6] Creating EBS network.. $RESET"
echo;

{
    docker network create --ingress --driver overlay $EBS_DOCKER_NETWORK_NAME && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$YELLOW ⚠  EBS network already exists. $RESET"
    echo;
}

#-----------------------------------------------------------------------------#

## Pull Docker images
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [4/6] Loading Docker images.. $RESET"
echo;

{
    bash ../../utility/pull_images.sh env && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$RED ☒  Error in loading Docker images. $RESET"
    echo;
    exit 1
}

#-----------------------------------------------------------------------------#

## Generate values for db.env
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [5/6] Generating values for database credentials.. $RESET"
echo;

## Check if there is argument passed
## If yes, db.env will be updated
## Argument $1 is the value of secret path
if [ $# -eq 0 ]; then
    echo;
    echo "$YELLOW Skipping the step. $RESET"
    echo;
else
    {
        SECRET_PATH=$1 ## Sample: ebs-dev-db/creds/readwrite-ba
        echo $SECRET_PATH
        sed -i -e "s|xxxx|$SECRET_PATH|g" db.template.env.tmpl
        VAULT_TOKEN=$VAULT_CLIENT_TOKEN consul-template -vault-addr=$HASHICORP_VAULT_URL \
            -template="db.template.env.tmpl:db.env" -once && {
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
        }
    } || {
        echo;
        echo "$RED ☒  Error in generating values for database credentials. $RESET"
        echo;
        exit 1
    }
fi

#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for Breeding Analytics stack and deploy

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [6/6] Deploying stack... $RESET"
echo;

{
    set -x
    env $(cat env db.env | grep ^[A-Z] | xargs) \
        docker stack deploy --with-registry-auth -c ba-stack.yml $EBS_DOCKER_STACK_NAME && {
            set +x
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
            echo "$GREEN ---------------------------------------------------- $RESET"
            echo "$GREEN ☑  Deployment is complete. $RESET"
            echo;
        }
} || {
    set +x
    echo;
    echo "$RED ☒  Error in deploying stack. Please run the script again. $RESET"
    echo;
}
