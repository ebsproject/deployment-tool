#!/usr/bin/env bash
#-----------------------------------------------------------------------------#
### Tagging images
# This script will pull retag and push all images within the EBS operating 
# environment

#-----------------------------------------------------------------------------#
#@author: RLPetrie (rlp243@cornell.edu)
#@author: pselby (ps664@cornell.edu)
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# setting bash environment

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces 
# an exit
set -x # xtrace: Similar to -v, but expands commands [to unset and hide 
# passwords us "set +x"]

# set -v # sets verbosity to high echoing commands before executing

#-----------------------------------------------------------------------------#

source $1 # parameters file [ebs_tagging.parameters]

#-----------------------------------------------------------------------------#
### Dockerhub login
# Will prompt if docker pass is set to "askme"

set +x # Turning down verbosity as the password is echoed to stdout
if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
fi
echo;

docker login -u $docker_login -p $DOCKER_HUB_PASSWORD;
echo;

set -x 

#-----------------------------------------------------------------------------#
### For loop that itterates through the $repos variable set in 
# ebs_tagging.parameters file

for repo_element in "${repos[@]}"; do

    repo_arr=($(echo $repo_element | tr ";" "\n"))
    origin=${repo_arr[0]}
    dest=${repo_arr[1]}

    docker pull $dest
    
done

#-----------------------------------------------------------------------------#

env $(cat *.env .env | grep ^[A-Z] | xargs) docker stack deploy --compose-file sg_config/sg_stack.yml gobii-stack

#-----------------------------------------------------------------------------#
