# ebs-swarm

This is the official repository of the EBS swarm stack that will deploy EBS applications on an operating environment and passed onto a CI/CD pipeline.


### Prerequisites
* Make sure you have a docker swarm cluster setup (typically comes with latest docker engine)

### Standard Usage

1. Verify `/.env` has correct versions/tags
1. Start the stack (currently, there is no way to pass the env file directly): `env $(cat .env | grep ^[A-Z] | xargs) docker stack deploy --compose-file docker-stack.yml ebs-stack`
1. Stop: `docker stack rm ebs-stack`
1. View whats currently online `docker service ps`

### Advanced Usage

TBD TBD TBD



### Useful Paths on web-node
* /data/gobii_bundle/config/gobii-web.xml
* /usr/local/tomcat/