#!/usr/bin/env bash
#-----------------------------------------------------------------------------#
# I tried a pure docker swarm solution but with how individual containers are 
# currently built, this is not possible. See each commands below for an
# explanation as to why they are necessary.
#
#@author: Kevin Palis <kdp44@cornell.edu>
#-----------------------------------------------------------------------------#

#postgres complains if the directory is not empty
#and git won't push this directory if the directory is empty
sudo chown -R $(id -u):$(id -g) b4r-config
rm -f b4r-config/b4r/data/.gitkeep

#deploy stack - simply comment this out and uncomment the next command when the EBS dockerhub account is ready
#env $(cat .env | grep ^[A-Z] | xargs) docker stack deploy --with-registry-auth --compose-file docker-stack.yml ebs-stack

#deploy with Roy's env file (when all those repos are ready) if even one of them is not, don't use this
env $(cat .env *.env | grep ^[A-Z] | xargs) docker stack deploy --with-registry-auth --compose-file docker-stack.yml ebs-stack
