#!/bin/bash

PG_DUMP_FILE=/data/simba/database/2019.02.22-analytics.pg.sql

psql -U postgres -h 127.0.0.1 -c "CREATE USER aadmin WITH SUPERUSER;"
psql -U postgres -h 127.0.0.1 -c "CREATE DATABASE analytics;"
psql -U postgres -h 127.0.0.1 analytics < $PG_DUMP_FILE
psql -U postgres -h 127.0.0.1 -c "ALTER USER aadmin WITH PASSWORD 'analyticsAdmin'"
