#!/usr/bin/perl -w
use lib "$ENV{'SIMBA_ROOT'}/perl";
use strict;
use JSON;
use simbaUtils;
use dbUtils;
use Data::Dumper;

# statDesign.pl -- script that creates and runs SBATCH
#                  script for statistical design.
# 2018.09.27
# Victor Jun M. Ulat
# v.ulat@cgiar.org

my %cfg=simbaUtils::readConfig();

my $cmd=''; # Use only when commands exceeds 60 columns.

# Open JSON file.
# If there are errors, make sure that they are 
# captured somewhere (perhaps an error log).

# get JSON sha
my $sha=simbaUtils::genShaFile($ARGV[0]);

# get JSON FH
open JSON, $ARGV[0] or die "Cannot open $ARGV[0]: $!"; 
my $data='';

# Read in contents to $data
while (my $line=readline *JSON){
    $data.=$line;
}

my $json=JSON::decode_json($data);

close JSON;

# Create output folder.
(my $tmpFldr=$ARGV[0])=~s/(.+)\/.+/$1/g;
$tmpFldr=~s/.+\///g;

(my $outFldr=$ARGV[0])=~s/.json$//ig;
$outFldr=~s/.+?\///g;

# Generate output prefix arg for the Rscript
my $outPref=$outFldr."_"; 
`mkdir $cfg{'wrd'}/$tmpFldr/$outFldr`;
`mv $ARGV[0] $cfg{'wrd'}/$tmpFldr/$outFldr/`;

# Record job submission
trackJob($ARGV[0], $sha);

# CreateSbatch script in the output dir...
# Return full path;
my $sh=createSbatch();

# Submit to SLURM
`sbatch $sh`;

exit();

sub trackJob {
  my ($reqName, $sha)=@_;
  $reqName=~s/.+\///g;
  $reqName=~s/\.JSON//g;
  (my $reqType=$reqName)=~s/.+?\_(.+)\_.+/$1/g;
  my $s=dbUtils::addAnalysis($reqName, $sha, $reqType);
  if ($s ne '1'){
    #! What to do?
    simbaUtils::writeLog("$0: $s");
  }
}


#! Check if required JSON elements are found.
sub createSbatch {
  #! We need to specifiy R directory for running Rscript.
  # Generate sbatch header...
    
  # Job id
  my $jobID=$outFldr.".0";
  # Prefix reqmnt for running module.
  my $outPref=$outFldr; # Prefix reqmnt for running module.
  # sh full path:
  my $fileSH="$cfg{'wrd'}/$tmpFldr/$outFldr/$jobID.sh";

  open SBATCH, ">$fileSH" or
          die "Cannot create $fileSH\n";

  print SBATCH "#!".$cfg{'bsh'}."\n";
  print SBATCH "#SIMBA v. 0.0002 SBATCH script\n\n";
    

  # Randomization method.
  print SBATCH "# Run randomization\n";

  # Get parameters.
  # Stat design model: requestMethod + ".R"
  # Should include version number to make it more
  # unique.

  my $r=$json->{metadata}->{requestMethod}.".R";
  my %p=simbaUtils::getParams(Dumper $json->{'parameters'});

  if (-e "$cfg{'mdl'}/statDesign/$r") {
    # Get all parameters.
    my $params='';
    foreach my $key (keys %p) {
      $params .= "--$key"." $p{$key} "; 
    }
    
    $params=~s/\s$//g;
    print SBATCH "\#SBATCH --job-name=$outFldr\n";
    print SBATCH "\#SBATCH --error=".
                 "$cfg{'lgd'}/$outFldr.err\n";
    print SBATCH "\#SBATCH --output=".
                 "$cfg{'lgd'}/$outFldr.out\n";
    # Track job start
    # NOTE: Since this is a stat design, parent job is 0 
    print SBATCH "$cfg{'bin'}/tracker.pl -t c ".
                 "-a $outFldr -j $jobID -p 0\n";
    print SBATCH $cfg{'rd1'}."/Rscript --vanilla ". 
          $cfg{'mdl'}. "/statDesign/$r ";
    print SBATCH $params. " ";
    print SBATCH "-o \"$outPref\" ".
          "-p \"$cfg{'wrd'}/$tmpFldr/$outFldr\"\n";
  } else {
    # Method is missing.
    #! Send message/log this error (simba.err)
    print SBATCH "WARNING: Method not found!\n";
    exit();
  }

  # Compress results to output.
  print SBATCH "\n# Compress results to output folder\n";
  print SBATCH "tar -czf $cfg{'out'}/".
               "$outFldr.tar.gz -C ".
               "$cfg{'wrd'}/$tmpFldr/$outFldr .\n";
  print SBATCH "rm -fr $cfg{'wrd'}/$tmpFldr/$outFldr\n";
  # track job end
  print SBATCH "$cfg{'bin'}/tracker.pl -t u ".
               "-j $jobID\n";

  #! send e-mail
  print SBATCH "\n# Send e-mail notification\n";
  close SBATCH;

  #! Return success!
  return("$fileSH");
}

