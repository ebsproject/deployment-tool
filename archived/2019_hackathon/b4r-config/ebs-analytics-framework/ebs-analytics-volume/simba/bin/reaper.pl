#!/usr/bin/perl -w
use lib "$ENV{'SIMBA_ROOT'}/perl";
use strict;
use simbaUtils;
use dbUtils;

# reaper.pl -- script that scans input folder for 
#              any analyses files. For each file
#              create an sbatch template and 
#              submit to slurm.
# 2018.09.27
# Victor Jun M. Ulat
# v.ulat@cgiar.org

# read config file
my %cfg=simbaUtils::readConfig();

my $cmd=''; # Use only when commands exceeds 60 columns.

# Create temp folder: folder name from current date time.
# Contains all the analyses requests.

my $tmpFldr=genFldrName();
  
my @analysisFiles=`ls -1 $cfg{'int'} -I "README"`;

if (@analysisFiles>0) {
  # Move all files to temp directory in the scratch folder.
  
  `mkdir $cfg{'wrd'}/$tmpFldr`;
  `mv $cfg{'int'}/*.* $cfg{'wrd'}/$tmpFldr`;

  foreach my $file (@analysisFiles) {
      # Do some thing for each files.
      # These files could be json (for stat design jobs) 
      # or json + input files (for analyses jobs).
      chomp $file;

      # Check if file has been submitted before: 
      my $filePath="$cfg{'wrd'}/$tmpFldr/$file";
      my $reqSha=simbaUtils::genShaFile($filePath);
      my $processed=dbUtils::getAnalysisId($reqSha, 1);

      if ($processed) {
          # File was already processed, delete.
          # Write to log.
          simbaUtils::writeLog(
                "$0: $file: Duplicate request, deleted.");
          `rm -fr $filePath`;
      } else {
          #! There should be logic here to determine if 
          #! request is for stat design or analyses.
          #! If file name contains a "_SD_", submit to
          #! statDesign.pl.
          if ($file=~/SD/) {
              # Request is for statistical design.
              # Run: bin/statdesign.pl
              $cmd="$cfg{'bin'}/statDesign.pl $filePath";
              `$cmd`;
          } elsif ($file=~/SA/) {
              # Request is for stat analysis.
              # Run: bin/statAnalysis.pl
          } else {
              # throw an error ...
              simbaUtils::writeLog("$0: Unknown error."); 
          }
      }
    }
}

sub genFldrName {
  my @t=localtime(time);
  $t[5]+=1900;
  $t[4]+=1; # Month is from 0 to 11
  my $fldrName=$t[5].sprintf("%02d", $t[4]).
               sprintf("%02d", $t[3]).
               $t[2].$t[1].sprintf("%02d", $t[0]);
  return($fldrName);
}
