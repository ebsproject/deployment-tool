#!/usr/bin/perl -w
use lib "$ENV{'SIMBA_ROOT'}/perl";
use strict;
use simbaUtils;
use dbUtils;
use Getopt::Std;

# tracker.pl -- script that tracks job(s) of an
#               analysis. Creates job, given an
#               analysis request and updates the
#               job after completion.
# 2019.02.21
# Victor Jun M. Ulat
# v.ulat@cgiar.org

# t - c (create), u (update) flags
# a - analysis id; required for -t create
# j - job name; required for -t create, -t update
# p - parent job id; required for  -t create
# s - job status; required for -t update
# e - err file; optional for -t update

# .sh should have the jobname (appended by .X)
# folder name should be request

my %args = ();
getopt("tajpse:", \%args);

my $argN = keys %args;

if ($argN==0){
  print "No arguments given.\n";
  exit();
}

if ($args{t} eq 'c'){
  if (($args{a}) && ($args{j}) && (defined $args{p})) {
    my $analysisID=dbUtils::getAnalysisId($args{a}, 0);
    if($analysisID) {
      my $created=dbUtils::addJob($analysisID, 
                                  $args{j}, $args{p});
    } else {
      print "Analysis: $args{a} is not in the database!\n";
    }
  } else {
    print "Please supply request_id, job name, parent_id.\n";
    exit(); 
  }
}

if ($args{t} eq 'u'){
  # check if 'j','p','s' are not null
  # check if optional 'e' is not null
  # update job j. 
  if ($args{j}){
    # get jobID
    my $jobID=dbUtils::getJobId($args{j});
    if ($jobID) {
      # job name exists
      # check if err msg exists
      (my $reqID=$args{j})=~s/\.\d+$//g;
      my $status=1;
      my $errMsg=simbaUtils::getErrMsg($reqID);
      if ($errMsg){
        $status=0;
      }
      my $updated=dbUtils::updateJob($args{j},
                                     $status, $errMsg);
    } else {
      print "Job: $args{j} is not in the database!\n";
    }
  }
}

exit();
