#!/usr/bin/perl -w
use strict;

# TODO's
# 1. err msg. for dependent tasks.
# 2. subtasks should be ran in parallel (same start, 
#    different end times.
# 3. parent job should have the time_end of the 
#    very last task.
# 4. analysis table should have time_completed and
#    status columns that reflects the time_end of the 
#    last completed jobs and its status.
#    CASES:
#    1. error -- no time_completed, status 0
#    2. jobs with dependencies did not complete.

open TBL, $ARGV[0] or die "Cannot open $ARGV[0]: $!";

my $idx=1;

while (my $row=readline *TBL) {
  chomp $row;
  genJob($row);
} 

close TBL;
exit();

sub genJob {
  my ($row)=@_;
  my @r=split(/\t/, $row);
  my $analysisId=$r[0];
  my $resultsFile=$r[1];
  my $analysisTimeStart=$r[4];
  my @status=(1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,
              1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
              1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
              1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
              1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
  my @lenght=(2,2,2,5,5,10,3,3,20,20,20,3,3,30);
  my @errChr=(" "," ",0..9," "," "," "," "," "," "," "," ",
              " "," ","A".."Z"," ","a".."z"," "," "," ");
  my $parentId=0;
  
  
  my $jobStatus=$status[rand @status];
  my $errMsg='';

  if ($jobStatus==0) {
    $errMsg="Err: ";
    $errMsg.=$errChr[rand @errChr] for 1..20;
    $errMsg=~s/\s{2,}/ /g;
    $errMsg=~s/^\s+//g;
  }

  my $elapsed='';

  if ($r[3] eq "SD") {
    $resultsFile.="_01_00";
    $elapsed=getEndTime($analysisTimeStart, 1);
    print $idx, "\t", $analysisId, "\t", $resultsFile, "\t",
          $analysisTimeStart, "\t", $elapsed, "\t",
          $parentId, "\t", $jobStatus, "\t",
          $errMsg, "\n";
    $idx++;
  } elsif ($r[3] eq "SA"){
    # sub jobs
    my @n=(2,2,10,10,5,2,3,3,3,3,3,6,6,4,5);
    my $subJobN=$n[rand @n];
    # dependent jobs (dependent job)
    my @w=(0,2,0,0,2,0,3,0,5,4,0,3,0,0);
    my $depJobN=$w[rand @w];
    my $prevTime=$analysisTimeStart;
    $parentId=$idx;
    for (my $x=1; $x<$subJobN; $x++){
       my $pId=0;
       my $rF=$resultsFile."_".sprintf("%02d", $x)."_00";
       my $analysisLenght=$lenght[rand @lenght];
       $elapsed=getEndTime($prevTime, $analysisLenght);
       print $idx, "\t",  $analysisId, "\t", $rF, "\t",
             $prevTime, "\t", $elapsed, "\t", $pId, "\t",
             $jobStatus, "\t", $errMsg, "\n";
       $prevTime=$elapsed;
       $idx++;
       if ($depJobN>0) {
            $rF="*".$rF;
          for (my $i=1; $i<=$depJobN; $i++) {
            if ($i==1){
              $pId=0;
            } else {
              $pId=($idx-$i)+1;
              $rF=~s/\_\d\d$/\_/g;
              my $depN=($i-1);
              $rF.=sprintf("%02d", $depN);
            }
            $analysisLenght=$lenght[rand @lenght];
            $elapsed=getEndTime($prevTime, $analysisLenght);
            print $idx, "\t", $analysisId, "\t", $rF, "\t",
                  $prevTime, "\t", $elapsed, "\t*", $pId, 
                  "\t", $depJobN, "\n";
            $prevTime=$elapsed;
            $idx++; 
          }
       }
    }
  }
}

sub getEndTime {
  my ($t,$l)=@_;
  my ($yyyymmdd,$hms)=split(/ /, $t);
  my ($yyyy, $mm, $dd)=split(/\-/, $yyyymmdd);
  my ($h, $m, $s)=split(/\:/, $hms);
  $s=~s/\.\d\d\d\d\d\d$//g;
  my $e=$m+$l;
  if ($e==60) {
    $m='00';
    $h=~s/^0//g;
    $h=sprintf("%02d", $h+1);
  } elsif ($e>60) {
    $m=sprintf("%02d", $e-60);
    $h=~s/^0//g;
    $h=sprintf("%02d", $h+1);
  } else {
    $m=sprintf("%02d", $e);
  }

  if ($h==24) {
    $dd=$dd+1;
    $h='00';
  }
  
  if ($dd>31) {
    $dd=1;
    $mm=~s/^0//g;
    $mm=sprintf("%02d", $mm+1);
  }

  if (($mm=='02') && ($dd>28)) {
    $mm='03';
    $dd='01';
  }

  if (($mm=='04') && ($dd>30)) {
    $mm='05';
    $dd='01';
  }

  if (($mm=='06') && ($dd>30)) {
    $mm='07';
    $dd='01';
  }

  if (($mm=='09') && ($dd>30)) {
    $mm='10';
    $dd='01';
  }

  if (($mm=='11') && ($dd>30)) {
    $mm='12';
    $dd='01';
  }

  if ($mm>12) {
    $mm='01';
    $yyyy=$yyyy+1;
  }   
  
  my @nu=(1..8);
  my $ms="$nu[rand @nu]"."$nu[rand @nu]"."$nu[rand @nu]".
         "$nu[rand @nu]"."$nu[rand @nu]"."$nu[rand @nu]";

  my $endTime=$yyyy."-".$mm."-".$dd. " ".
              $h.":".$m.":".$s.".".$ms;
  return($endTime);
}
