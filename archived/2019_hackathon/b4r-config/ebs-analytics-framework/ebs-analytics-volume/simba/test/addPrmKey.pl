#!/usr/bin/perl -w
use strict;

open TBL, $ARGV[0] or die "Cannot open $ARGV[0]: $!";

my $idx=0;

while (my $row=readline *TBL) {
  $idx++;
  $row=~s/02-31/02-28/g;
  $row=~s/02-30/02-28/g;
  $row=~s/02-29/02-28/g;
  $row=~s/04-31/04-30/g;
  $row=~s/06-31/06-30/g;
  $row=~s/09-31/09-30/g;
  $row=~s/11-31/11-30/g;
  print $idx, "\t", $row;
}

close TBL;
exit();
