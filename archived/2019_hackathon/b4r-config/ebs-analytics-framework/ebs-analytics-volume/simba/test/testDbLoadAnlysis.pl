#!/usr/bin/perl -w
use lib '../perl';
use dbUtils;
use strict;

my $q1=$ARGV[0];
my $q2=$ARGV[1];
my $q3=$ARGV[2];

# requestId, sha, request type
# my $is_added=dbUtils::addAnalysis($q1, $q2, $q3);

# analysisId, jobName
# my $is_added=dbUtils::addJob($q1, $q2);

# analysisId, jobName, parentId
my $is_added=dbUtils::addSubJob($q1, $q2, $q3);

if ($is_added eq 1) {
  print "Successfully added $q2.\n";
} else {
  print "Error adding $q2: $is_added\n";
}

exit();
