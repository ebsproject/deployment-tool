#!/usr/bin/perl -w
use strict;

my $rowNum=$ARGV[0];

for (my $i=0; $i<$rowNum; $i++) {
  genRow();
}

sub genRow {
  my @num=(0..9);
  my @chr=("A".."Z");
  my @mix=(0..9,0..9,"A".."Z",0..9,0..9,"a".."z",0..9,0..9);
  my @dy=(1..31);
  my @mo=(1..12);
  my @hr=(0..23);
  my @mn=(1..59);
  my @nu=(1..8);
  my @rq=("SD","SD","SA","SD","SD","SD","SA","SD","SD",
          "SA","SD");
  
  my $ms="$nu[rand @nu]"."$nu[rand @nu]"."$nu[rand @nu]".
         "$nu[rand @nu]"."$nu[rand @nu]"."$nu[rand @nu]";

  my $timeSubmitted='2018-';
  $timeSubmitted.=sprintf("%02d", $mo[rand @mo]);
  $timeSubmitted.="-";
  $timeSubmitted.=sprintf("%02d", $dy[rand @dy]);
  $timeSubmitted.=" ";
  $timeSubmitted.=sprintf("%02d", $hr[rand @hr]);
  $timeSubmitted.=":";
  $timeSubmitted.=sprintf("%02d", $mn[rand @mn]);
  $timeSubmitted.=":";
  $timeSubmitted.=sprintf("%02d", $mn[rand @mn]);
  $timeSubmitted.=".";
  $timeSubmitted.=$ms;
 
  my $requestID="2018";
  my $requestType=$rq[rand @rq];
  $requestID.=$num[rand @num] for 1..4;
  $requestID.=$chr[rand @chr] for 1..3;
  if ($requestType eq "SD") {
    $requestID.="_SD_";
  } else {
    $requestID.="_SA_";
  }
  $requestID.=$num[rand @num] for 1..2;
  $requestID.=$chr[rand @chr] for 1..2;
  #$requestID.=".JSON";
  my $sha1='';
  $sha1.=$mix[rand @mix] for 1..40;

  print $requestID, "\t";
  print $sha1, "\t";
  print $requestType, "\t";
  print $timeSubmitted, "\t";
  print "\n";
}
