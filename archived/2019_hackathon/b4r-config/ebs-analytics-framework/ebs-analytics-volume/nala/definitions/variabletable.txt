Parameter	Description
nTrial	number of location reps (trials)
nTreatment	number of entries (RCB, Alpha Lattice and Row-Column designs)
nCheckTreatment	number of checks or replicated entries (Augmented RCBD)
nTestTreatment	number of test or unreplicated entries (Augmented RCBD)
nRep	number of replicates (or blocks, in the case of RCBD)
nBlk	number of blocks (Alpha Lattice design)
nRowBlk	number of row blocks (Row-Column design)
genLayout	if TRUE, layout will be generated
nFieldRow	number of field rows, required if genLayout is TRUE
nRowPerRep	number of rows per replicate, required if genLayout is TRUE (RCBD and Alpha Lattice design)
nRowPerBlk	number of rows per block, required if genLayout is TRUE (Alpha Lattice design)
serpentine	if TRUE, plot numbers will be in serpentine arrangement, required if genLayout is TRUE
outputFile	prefix to be used for the names of the output files
outputPath	path where output will be saved
