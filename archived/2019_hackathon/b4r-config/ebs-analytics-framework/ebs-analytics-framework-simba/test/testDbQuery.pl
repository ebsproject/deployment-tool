#!/usr/bin/perl -w
use lib '../perl';
use simbaUtils;
use dbUtils;
use strict;

my %cfg=simbaUtils::readConfig();

my $dbh=dbUtils::openDB();

if ($dbh) {

  my $sql="SELECT id ".
          "FROM job ".
          "WHERE analysis_id=1000003 ".
          "AND parent_id=0";
  my $sth=$dbh->prepare($sql);
  
  my $rv=$sth->execute() or die $DBI::errstr;

  if ($rv < 0) {
    print $DBI::errstr;
  }  
 
  my $i=0;
  my $id='';

  while (my @row = $sth->fetchrow_array()) {
    $i++;
    $id=$row[0];
  }

  $sth->finish;
  
  if ($i>1){
    print "Error: multiple ids found  ".
          "should only be id:2216878.\n";
  } else {
    print $i,":",$id, " Opened database successfully!\n";
  }
  
}


