package simbaUtils;
# simba's utilities module

use strict;
use warnings;

my $conf="$ENV{'SIMBA_ROOT'}/conf/simba.conf";
my %cfg=readConfig();

sub readConfig {
  open CONF, $conf or die "Cannot open $conf: $!";
  my %cfg=();
  while (my $line=readline *CONF){
    next unless $line !~ /^\#/;
    next unless $line !~ /^$/;
    chomp $line;
    my ($key, $value)=split(/\=/, $line);
    if (exists($cfg{$key})){
      print "Config error: Duplicate entry for $key!\n";
      exit();
    } else {
      $cfg{$key}=$value;
    }
  }
  return(%cfg);
}

sub getParams {
  my ($str)=@_;
  my %p=();
  my @tmp=split(/\n/, $str);
  foreach my $tmp(@tmp) {
    $tmp=~s/^\s*//g;
    if ($tmp=~/^\'/){
      $tmp=~s/\,$//g;
      my ($k, $v)=split(/ \=\> /, $tmp);
      $k=~s/\'//g;
      $v=~s/\'//g;
      if (exists($p{$k})) {
         print "Error: Duplicate parameter name!\n";
         exit();
      } else {
         $p{$k}=$v;
      }
    }
  }
  return(%p);
}

sub genSha {
  my ($str)=@_;
  my $sha=`echo -n "$str" | sha1sum`;
  chomp $sha;
  $sha=~s/ .+//g;
  return ($sha);
}

sub genShaFile {
  my ($path)=@_;
  my $sha=`sha1sum $path`;
  chomp $sha;
  $sha=~s/ .+//g;
  return ($sha);
}

sub writeLog {
  my ($str)=@_;
  chomp $str;
  my $date=getDate();

  if (!-e $cfg{'rlg'}){
    # simba.LOG does not exist; create
    `touch $cfg{'slg'}`;
  }

  open LOG, ">>$cfg{'slg'}" 
              or die "Cannot open $cfg{'slg'}: $!";
  print LOG $date, "\t", $str, "\n";
  close LOG;
}

sub getDate {
  my $date=`date`;
  chomp $date;
  return ($date);
}

sub getErrMsg {
  my ($errFile)=@_;
  $errFile.=".err";
  my $msg='';

  if (!-s "$cfg{'lgd'}/$errFile"){
    # file is empty
    $msg=0;
  } else {
    open ERR, "$cfg{'lgd'}/$errFile"
         or die "Cannot open $cfg{'lgd'}/$errFile";
    while (my $errLine=readline *ERR){
      $msg.=$errLine;
    }
  }
  return($msg);
}

1;
