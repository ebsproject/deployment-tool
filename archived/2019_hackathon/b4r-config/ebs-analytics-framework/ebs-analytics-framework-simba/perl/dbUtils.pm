package dbUtils;
# simba's database utilities module
use lib "$ENV{'SIMBA_ROOT'}/perl";
use strict;
use warnings;
use simbaUtils;
use DBI;

my %cfg=simbaUtils::readConfig();

sub openDB {
  my $dsn="DBI:Pg:dbname=$cfg{'dbn'};".
                     "host=$cfg{'dbh'};".
                     "port=$cfg{'dpt'}";
  my $dbh=DBI->connect($dsn, $cfg{'dbu'}, 
                       $cfg{'dbp'}, {RaiseError=>1})
               or die $DBI::errstr;
  return($dbh);
}

sub closeDB {
  my ($dbh)=@_;
  $dbh->disconnect;
}

sub addAnalysis {
  # Inserts new analysis.
  # Requires: request ID, sha and request type.
  # Returns: 1 for success; 0 for failure.
  my ($reqId, $sha, $reqType)=@_;
  
  # Check if analysis exists (already loaded).
  my $exists=getAnalysisId($sha, 1);
  
  if ($exists) {
    return("$reqId already in the database.")
  } else {
    my $dbh=openDB();
    my $sql='';

    $sql="INSERT INTO analysis (".
            "request_id, sha, request_type, ".
            "time_submitted".
         ") VALUES (".
            "\'$reqId\', \'$sha\', \'$reqType\', ".
            "current_timestamp".
         ")";

    my $sth=$dbh->prepare($sql);
    my $rv=$sth->execute() or die $DBI::errstr;

    $sth->finish;
    closeDB($dbh);

    if ($rv< 0){
      return(0);
    } else {
      return(1);
    }
  }
}

sub addJob {
  # Inserts new job.
  # Requires: analysis.id, job.name, parent id.
  # Returns: 1 for success; 0 for failure.
  my ($analysisId, $jobName, $parentId)=@_;
   
  my $exists=getJobId($analysisId, $jobName);

  if ($exists) {
    return("$jobName already in the database.")
  } else {
    my $dbh=openDB();
    my $sql='';
    $sql="INSERT INTO job (".
           "analysis_id, name, time_start, parent_id".
         ") VALUES (".
           "\'$analysisId\', \'$jobName\', ".
           "current_timestamp, \'$parentId\'".
         ")";

    my $sth = $dbh->prepare($sql);
    my $rv = $sth->execute() or die $DBI::errstr;

    if ($rv < 0) {
      print $DBI::errstr;
      return(0);
    } else {
      return(1);
    }
    closeDB($dbh);
  }
}

sub updateJob {
  # Updates given job name.
  # Requires: job name, status and/or err_msg
  # Returns: 1 for success; 0 for failure.
  my ($jobName, $status, $err_msg)=@_;
  my $dbh=openDB();
  my $sql='';
  if ($err_msg) {
    $sql="UPDATE job ".
         "SET ". 
            "time_end = current_timestamp, ".
            "status = \'$status\', ".
            "err_msg = \'$err_msg\'".
         "WHERE ".
            "name=\'$jobName\'";
  } else {
    $sql="UPDATE job ".
         "SET ". 
            "time_end = current_timestamp, ".
            "status = \'$status\' ".
         "WHERE ".
            "name=\'$jobName\'";
  }
  my $sth=$dbh->prepare($sql);
  my $rv=$sth->execute() or die $DBI::errstr;
  if ($rv < 0) {
    print $DBI::errstr;
    return(0);
  } else {
    return(1);
  }
  closeDB($dbh);
}

sub addSubJob {
  # Inserts new sub job (sub task).
  # Requires: analysis id, job.id.
  # Returns: 1 for success; 0 for failure.
  my ($analysisId, $jobName, $parentId)=@_;

  my $exists=getJobId($analysisId, $jobName);

  if ($exists) {
    return("$jobName already in the database.");
  } else {
    my $dbh=openDB();
    my $sql='';
    $sql="INSERT INTO job (".
           "analysis_id, name, time_start, parent_id".
         ") VALUES (".
           "\'$analysisId\', \'$jobName\', ". 
           "current_timestamp, \'$parentId\'".
         ")";

    my $sth = $dbh->prepare($sql);
    my $rv = $sth->execute() or die $DBI::errstr;

    if ($rv < 0) {
      print $DBI::errstr;
      return(0);
    } else {
      return(1);
    }
    closeDB($dbh);
  }
}

sub getStatusAnalysis {
  # Returns 1 if analysis status is complete (1, all jobs
  # completed).
  # Requires: sha or request_id.
  # check if analysis exists first.

  my ($q, $is_sha)=@_;  

  my $exists=getAnalysisId($q, $is_sha);
  
  if ($exists) {
    # sha/request)id exists.
    # Check status.

    my $dbh=openDB();
    my $sql=0;

    if ($is_sha) {
      $sql="SELECT count(*) ".
           "FROM analysis a, job j ".
           "WHERE a.sha=\'$q\' ".
           "AND a.id=j.analysis_id ".
           "AND j.status=0";
    } else {
      $sql="SELECT count(*) ".
           "FROM analysis a, job j".
           "WHERE a.request_id=\'$q\' ".
           "AND a.id=j.analysis_id ".
           "AND j.status=0";
    }

    my $sth=$dbh->prepare($sql);
    my $rv=$sth->execute() or die $DBI::errstr;

    if ($rv < 0) {
      print $DBI::errstr;
    }

    my ($count)=$sth->fetchrow_array();

    if ($count==0) {
       # all jobs completed successfully.
       return(1);
    } else {
       # All/some jobs failed.
       # For analysis request with only one job
       # this is a complete failure.
       #! What to do?
       return(0);
    }

    closeDB($dbh);

  } else {

    # Does not exists, sha/request id not found.
    return(0);

  }
}

sub getAnalysisId {
  # Returns analysis.id given request_id or sha.
  # Requires: sha or request_id.
  my ($q, $is_sha)=@_;  

  my $dbh=openDB();

  my $sql='';

  if ($is_sha) {
    $sql="SELECT id ".
         "FROM analysis ".
         "WHERE sha=\'$q\'";
  } else {
    $sql="SELECT id ".
         "FROM analysis ".
         "WHERE request_id=\'$q\'";
  }
  my $sth=$dbh->prepare($sql);
  my $rv=$sth->execute() or die $DBI::errstr;

  if ($rv < 0) {
    print $DBI::errstr;
  }

  my $i=0;
  my $id='';

  while (my @row=$sth->fetchrow_array()) {
    $i++;
    $id=$row[0];
  }

  if ($i>1) {
    return("Error: result is more than 1");
  } else {
    if ($id) {
      return($id);
    } else {
      return(0);
    }
  }

  closeDB($dbh);

}

sub getJobId {
  # Returns job.id
  # Requires: job.name
  my ($jobName)=@_;

  my $dbh=openDB();

  my $sql='';

  $sql="SELECT j.id ".
       "FROM job j ".
       "WHERE j.name=\'$jobName\'";
  
  my $sth = $dbh->prepare($sql);
  my $rv=$sth->execute() or die $DBI::errstr;

  if ($rv < 0) {
    print $DBI::errstr;
  }

  my $i=0;
  my $jId='';

  while (my @row = $sth->fetchrow_array()) {
    $i++;
    $jId=$row[0];
  }

  if ($i>1) {
    return("Error: result is more than 1");
  } else {
    if ($jId) {
      return($jId);
    } else {
      return(0);
    }
  }
  
  closeDB($dbh);

}

  
  
# get analysis.id given a request_id
# get job.id(s) given an analysis.id
# get "main" job.id given an analysis.id
# insert analysis
# insert job, given analysis.id
# insert job, given parent job.id
# update job
# update analysis
# get all jobs given analysis id
# get subtasks given job_id
# get status given job_id

1;
