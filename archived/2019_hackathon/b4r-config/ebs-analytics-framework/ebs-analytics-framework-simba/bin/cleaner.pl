#!/usr/bin/perl -w
use lib "$ENV{'SIMBA_ROOT'}/perl";
use strict;
use simbaUtils;

# cleaner.pl -- script that scans logs folder for
#               any "empty" files and deletes them.
#               This should be set as a cron job.
# 
# 2019.02.12
# Victor Jun M. Ulat
# v.ulat@cgiar.org

# read config file
my %cfg=simbaUtils::readConfig();

# Find files (.err and .out) that are 30 days old
# and delete if they are empty.
# my @files=
#    `find $cfg{'lgd'}/ -type -f -name '*.*' -mtime +30`;

my @files=`ls -1 $cfg{'lgd'}/ -I "simba.LOG" -I "README"`;
my $filesN=scalar @files;
my $deleted=0;

foreach my $file (@files) {
  chomp $file;
  if (!-s "$cfg{'lgd'}/$file"){
     `rm -fr $cfg{'lgd'}/$file`;
     $deleted++;
  }
}

my $txt="$0: Found $filesN file(s) in $cfg{'lgd'}, ".
        "deleted $deleted empty file(s).\n";

simbaUtils::writeLog($txt);

# clean scratch folder of empty directories

my @dirList=`ls -1 $cfg{'wrd'}/ -I "README"`;
my $foldersN=scalar @dirList;
$deleted=0;

foreach my $dir (@dirList) {
  chomp $dir;
  if (-d "$cfg{'wrd'}/$dir"){
    my @list=`ls -1 $cfg{'wrd'}/$dir`;
    if (@list==0){
      `rm -fr $cfg{'wrd'}/$dir`;
      $deleted++;
    }
  }  
}

$txt="$0: Found $foldersN folder(s) in $cfg{'wrd'}, ".
     "deleted $deleted empty folder(s).\n";

simbaUtils::writeLog($txt);

exit();
