#!/bin/bash

POSTGRES_CONTAINER=ebs-af-postgres
INIT_SCRIPT=config/init_db.sh

docker cp $INIT_SCRIPT $POSTGRES_CONTAINER:/

docker exec -it $POSTGRES_CONTAINER /bin/bash -c "sh $INIT_SCRIPT"
