## Introduction
Rafiki is the primary infrastructure being utilized by the EBS Analytics Framework. It includes Dockerfile definitions for `Slurm` and `R` as well as a Docker Compose file for orchestrating the deployment of these components.

## Prerequisites
1. Docker Hub account
repository.
2. Access to the `irribim` Docker Hub account.
3. Access to the `nala` repository.
4. Access to the `simba` repository.


## Installation Guide

Clone `simba` and `nala` inside the project directory.

```
git clone <SIMBA GIT REPOSITORY>
```

Run `01_init_volume.sh`. This will create a named Docker volume and copy all the necessary files that the rest of the components will share.

```
sh 01_init_volume.sh
```

Login to Docker Hub.

```
docker login -u <USERNAME>
```

Execute the Docker Compose file.

```
// Run in the foreground
docker-compose up

//Run in the background
docker-compose up -d
```

Initialize the database.

```
sh 02_init_db.sh
```

**NOTE:** For succeeding runs, you do not need to execute the init scripts.

## Integrating with B4R

Modify the B4R Docker Compose file so that the volume is defined and the volume is mounted in the container.
```
volumes:
  ebs-analytics-volume:
    external: true
services:
...
b4r-web-yii2:
    ...
    volumes:
      - ebs-analytics-volume:/data
    ...
```
`/data` contains the following directories:

* input
* output
* simba
* nala
* postgres
* scratch