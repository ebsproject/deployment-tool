#!/bin/bash

VOLUME_NAME=ebs-analytics-volume

## Create a Docker named volume
docker volume create --name $VOLUME_NAME

docker run -itd -v $VOLUME_NAME:/data --name storage-service alpine:latest

# Create directories to be used by the analysis
docker exec -it storage-service sh -c "cd /data && mkdir -p input && mkdir -p output && mkdir scratch"

# Create data directory for PostgreSQL
docker exec -it storage-service sh -c "cd /data && mkdir -p postgres"

# Copy simba and nala to the volume
docker cp simba/ storage-service:/data/
docker cp nala/ storage-service:/data/

docker rm -f storage-service
