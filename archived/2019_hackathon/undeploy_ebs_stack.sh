#!/usr/bin/env bash
#-----------------------------------------------------------------------------#
# An undeploy script to complement the deploy script.
#
#@author: Kevin Palis <kdp44@cornell.edu>
#-----------------------------------------------------------------------------#
#fix ownership so git can work again
sudo chown -R $(id -u):$(id -g) b4r-config

#undeploy the stack
docker stack rm ebs-stack