# Utility Node 

_This is a set of tools that will be used to create a untility node.  This node was created to consolidate installs and functions to help deploy and configure the swarm cluster._

-------------------------------------------------------------------------------

### **Container Creation [ util.dockerfile ]**
**This is file should be kept up-to-date with latest installs for the following:**

• Basic Installs (apt install):
```bash
  ↳ vim
  ↳ git
  ↳ htop
  ↳ tree
  ↳ time
  ↳ sudo
  ↳ sed
  ↳ wget
  ↳ curl
  ↳ libssl-dev
  ↳ unzip
  ↳ cowsay
  ↳ fortune
  ↳ tmux
  ↳ openssh-server
```
The install for 'tzdata' is interactive and breaks automation.  Use the following to use defaults.  This install is required for postgresql:
```bash
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
```
  

The next three installs are the processes used as of the last time this was updated for docker, docker-compose and postgresql.

  

#### **Local user**
This is a local user in the container that will be used rather than using the root user.  Configurations for the users profile are copied over for .vimrc, .tmux.conf and .bashrc to help simplify container user usage.

  

#### Ports
Expose the following ports for ssh and postgresql:
```bash
EXPOSE 22
EXPOSE 5432
EXPOSE 5434
```

  

#### Entrypoint
The entrypoint script is created during image build to restart ssh and run the environment bash to keep the container running. (then chmoded for all user access)

-------------------------------------------------------------------------------
  

## util-build.sh

This script will be used to trigger the automation build of the docker image using the util.docker file.

#### Usage
The script uses the following format for triggering automation.
```bash
bash <bash_script_name>.sh <Docker Image Push \"true\" or \"false\"> <Dockerhub Login Username> <DOCKERHUB_PASSWORD_(optional)>
```

**Example:**
```bash
bash util-build.sh true lin123 pass321
```

Each value has a specific purpose:
```bash
# (* Required) This is used so if you would like to just build the image locally set this to false
# If this is set to true the image will be created then pushed to the dockerhub repo
<Docker Image Push \"true\" or \"false\">

# (Otiontional) This is the Dockerhub user that has access to the repo for pushing the image(s) after creation
<Dockerhub Login Username>

# (Otiontional) This is the password.  When using in automation this should be used as a password variable so it is confuscated. Addtionally this will trigger a prompt is specifically set to 'askme'.  This should not be used in automation as the prompt will break an automation suite.
<DOCKERHUB_PASSWORD_(optional)>
```
  

When running the build the tag used for uniqueness is the current git branch the repo is using + Date + time.
```bash
TAG=$(date +"%Y%m%d_%H%M%S")
BRANCH=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p' | sed -e 's/\//_/g')
BUILD_TAG="$BRANCH"_$TAG
```
-------------------------------------------------------------------------------
  

## util-run.sh

This script will be used to trigger the deployment automation of the utility container.  

To run you will need the image tag and ran as following:
```bash
bash <bash_script_name>.sh <Dockerhub Tag>
```

**Example:**
```bash
bash util-run.sh feature_DEVOPS-185_SplitUtilRun_20210316_160907
```

⚠️ **_This script has a lot of vars hardcoded into it as it is used specifically for EBS deployment-tool repo. These scripts can be duplicated and used in other repos and environments but the values for the repo vars must be updated or created as dynamic vars used by deployment tool variables (bamboo, jenkins, etc.)_**

#### User run UID
This script will utilize the user ID of the user that deploys/triggers the script with the following.
```bash
docker exec -u root util-node bash -c "
find / -user 1000 -exec chown -h $UID {} \; || :
" || true;
```
This will find all items with user ID 1000 which is the default user created in the image and changes it to the triggered user.  This will make sure the internal user within the container matches the user on the host.

The end of the run script shuts down psql as the service should not hold files or services when another DB attempts to access those same files or services.