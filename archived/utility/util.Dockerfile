# Which OS base Image is being used
FROM ubuntu:18.04

#-----------------------------------------------------------------------------#
# Perform as root

USER root

#-----------------------------------------------------------------------------#
# Update and install needed software
RUN apt-get update && \
	apt-get install -y vim git htop tree time sudo sed wget curl libssl-dev unzip cowsay fortune tmux openssh-server;

RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata

#-----------------------------------------------------------------------------#
# Install the latest Docker CE binaries and add `jenkins` to the docker group

RUN apt-get update && \
    apt-get -y --no-install-recommends install apt-transport-https \
      ca-certificates \
      curl \
      gnupg2 \
      software-properties-common && \
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
    add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
      $(lsb_release -cs) \
      stable" && \
   apt-get update && \
   apt-get -y --no-install-recommends install docker-ce && \
   apt-get clean

#-----------------------------------------------------------------------------#
# install docker compose

RUN curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose && \
    ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

#-----------------------------------------------------------------------------#
# install psql

RUN echo "installing postgres" && \
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && \
	echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list && \
	sudo apt-get -y update && \
	sudo apt -y install postgresql-12 postgresql-client-12 && \
	sudo sed -i "s/local   all             postgres                                peer/local   all             postgres                                     peer\nlocal   all             all                                     md5/" /etc/postgresql/12/main/pg_hba.conf && \
  	sudo sed -i "s/host    all             all             127.0.0.1\/32            md5/host    all             all             0.0.0.0\/0            md5/" /etc/postgresql/12/main/pg_hba.conf && \
  	sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf && \
  	sudo service postgresql restart;

#-----------------------------------------------------------------------------#
# set up non-root user
RUN useradd -m gygax && \
  usermod -aG docker gygax && \
  echo 'gygax:gary' | chpasswd && \
  echo 'postgres:postgres' | chpasswd && \
  echo "gygax ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
  echo "postgres ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
  usermod --shell /bin/bash gygax

COPY .bashrc /home/gygax/.bashrc
COPY .tmux.conf /home/gygax/.tmux.conf
COPY .vimrc /home/gygax/.vimrc

COPY .bashrc /root/.bashrc
COPY .tmux.conf /root/.tmux.conf
COPY .vimrc /root/.vimrc

COPY .bashrc /var/lib/postgresql/.bashrc
COPY .tmux.conf /var/lib/postgresql/.tmux.conf
COPY .vimrc /var/lib/postgresql/.vimrc

#-----------------------------------------------------------------------------#
# expose service ports

EXPOSE 22
EXPOSE 5432
EXPOSE 5434

#-----------------------------------------------------------------------------#

#USER root
RUN touch /docker-entrypoint.sh && \
  echo '#!/usr/bin/env bash' >> /docker-entrypoint.sh && \
  echo "service ssh restart" >> /docker-entrypoint.sh && \
  echo "/bin/bash" >> /docker-entrypoint.sh && \
  chmod 777 /docker-entrypoint.sh

#-----------------------------------------------------------------------------#
# drop back to the regular gygax user - good practice

#USER gygax #! user can not run entrypoint to start services

#-----------------------------------------------------------------------------#
# set entrypoint for service start

ENTRYPOINT ["bash", "/docker-entrypoint.sh"]
