#!/usr/bin/env bash

#-----------------------------------------------------------------------------#

TAG=$(date +"%Y%m%d_%H%M%S");

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#

if [ $# -lt 1 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <Dockerhub Tag>"
    exit 1
fi

#-----------------------------------------------------------------------------#

DOCKER_HUB_ORG="ebsproject"
DOCKER_HUB_REPO_NAME="ebs-deployment-tool-builder"
DOCKER_HOST_GID=$(getent group docker | awk -F: '{printf $3}')
BUILD_TAG=$1

#-----------------------------------------------------------------------------#

cd ../

pwd="`pwd`" && echo $pwd

docker run -dti \
--name util-node \
-p 2345:22 \
-h util-node \
-v $pwd:/home/gygax/workspace/deployment-tool/ \
-v /var/run/docker.sock:/var/run/docker.sock \
--restart=always \
--privileged \
$DOCKER_HUB_ORG/$DOCKER_HUB_REPO_NAME:$BUILD_TAG
echo;

#-----------------------------------------------------------------------------#
### restart psql

# docker exec -u postgres util-node bash -c "
# service postgresql restart
# " || true;

# docker exec -u root util-node bash -c "
# service ssh restart
# " || true;

#-----------------------------------------------------------------------------#
### update all gygax user owned files to match uid of host user

docker exec -u root util-node bash -c "
find / -user 1000 -exec chown -h $UID {} \; || :
" || true;
echo;

#-----------------------------------------------------------------------------#
### Specifically set internal docker gid to docker gid on host & restart node

docker exec -u root util-node bash -c "
groupmod -g $DOCKER_HOST_GID docker
" || true;
echo;

echo "Restarting util-node setting docker gid to $DOCKER_HOST_GID"
docker restart util-node
echo;

#-----------------------------------------------------------------------------#
### Stop Postgres so not to collide with other DBs

docker exec -u root util-node bash -c "
service postgresql stop
" || true;
echo;

#-----------------------------------------------------------------------------#
### Completion

echo "Utility Container deployment has completed!"
echo;
