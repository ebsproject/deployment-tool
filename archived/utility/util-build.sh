#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message
set -x # Verbose with commands displayed

#-----------------------------------------------------------------------------#
### verify proper arguments are passed

if [ $# -lt 1 ];
  then
    echo "No arguments supplied."
    echo "Usage: bash <bash_script_name>.sh <Docker Image Push \"true\" or \"false\"> <Dockerhub Login Username> <DOCKERHUB_PASSWORD_(optional)>"
    echo "If password set to \"askme\" the script will prompt for concealed pass to be entered."
    exit 1
fi

#-----------------------------------------------------------------------------#
### Vars

TAG=$(date +"%Y%m%d_%H%M%S")
BRANCH=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p' | sed -e 's/\//_/g')
BUILD_TAG="$BRANCH"_$TAG
DOCKER_HUB_ORG="ebsproject"
DOCKER_HUB_REPO_NAME="ebs-deployment-tool-builder"
PUSH_IMAGE=$1 # PUSH_IMAGE=false Set to 'true' if pushing artifact
set +u
if [[ -n "${2}" ]]; then DOCKER_HUB_LOGIN_USERNAME="$2"; else echo "Docker Hub Login Username not passed into script via script arg. --> \$2"; fi
echo;
set -u

#-----------------------------------------------------------------------------#
### Build container from ubuntu

docker build -f util.Dockerfile -t $DOCKER_HUB_ORG/$DOCKER_HUB_REPO_NAME:$BUILD_TAG .
echo;

#-----------------------------------------------------------------------------#
### 

set +x

set +u
if [[ -n "${3}" ]]; then DOCKER_HUB_PASSWORD="$3"; else echo "Docker Hub Password not passed into script via script arg. --> \$3"; fi
echo;
set -u

set -x

#-----------------------------------------------------------------------------#
### 

if [[ "$PUSH_IMAGE" = true ]]; then
  #--------------------------------------#
  # log into dockerhub
  set +x # Turning down verbosity as the password is echoed to stdout
  docker logout

  if [ $DOCKER_HUB_PASSWORD = "askme" ]; then
    read -sp "Please enter your docker hub user password for [$DOCKER_HUB_LOGIN_USERNAME]: " DOCKER_HUB_PASSWORD
  fi
  echo;

  echo $DOCKER_HUB_PASSWORD | docker login -u $DOCKER_HUB_LOGIN_USERNAME --password-stdin
  set -x

  time docker push $DOCKER_HUB_ORG/$DOCKER_HUB_REPO_NAME:$BUILD_TAG
else
  echo "Not pushing image to Docker hub."
  echo;
fi

#-----------------------------------------------------------------------------#
