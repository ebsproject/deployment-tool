#!/bin/bash

## Validates the Docker images in a release version
##
## Instructions:
##    1. Update the FILENAME based on the release version
##          FILENAME=releases/official.yml
##    2. Run the script
##          bash validate_docker_images.sh
##
## Prerequisite: 
##  - Install yq
##    - For Ubuntu users: 
##          sudo apt-get install -y yq
##    - For macOS / Linux users using Homebrew:
##          brew install yq


FILENAME=releases/official.yml

#-----------------------------------------------------------------------------#
## Check if yq is installed
if ! command -v jq &> /dev/null
then
    echo "Please install yq first."
fi

#-----------------------------------------------------------------------------#

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
## Retrieve running images
docker service ls --format "{{.Image}}" > temp


#-----------------------------------------------------------------------------#

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Validating the Docker images... $RESET"
echo;

## Loop the yml file
while read -r first; 
    do read second; 

    ## Retrieve the Docker images
    IMAGE_VAR=`echo $first | cut -d ":" -f1`
    IMAGE_TAG_VAR=`echo $second | cut -d ":" -f1`

    ## Validate if the Docker images are in the yml file
    IMAGE=$(yq eval .${IMAGE_VAR} "$FILENAME")
    IMAGE_TAG=$(yq eval .${IMAGE_TAG_VAR} "$FILENAME")

    if grep -Fxq "$IMAGE:$IMAGE_TAG" temp
    then
        echo "$BLUE $IMAGE_VAR $RESET $GREEN ☑ $IMAGE:$IMAGE_TAG $RESET"
    else
        echo "$BLUE $IMAGE_VAR $RESET $RED ☒  Should be $IMAGE:$IMAGE_TAG $RESET"
    fi
done < $FILENAME
