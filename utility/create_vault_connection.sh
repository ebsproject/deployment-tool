#!/usr/bin/env bash

## Enable the database secrets engine in Vault
## Create database connection in Vault
##
## Instructions: 
##    1. Update the variables below
##    2. Run the script
##          bash create_vault_connection.sh


##--------------##
## Variables ##

VAULT_TOKEN="xxxx"
VAULT_ADDR="xxxx"
NEW_ENGINE="xxxx"
DB_NAME="xxxx"
DB_PASSWORD="xxxx"
DB_HOST="xxxx"
DB_PORT="xxxx"

##--------------##

## Enable the database secrets engine
{
    set -x
    curl --header "X-Vault-Token: ${VAULT_TOKEN}" \
       --request POST --data '{"type":"database"}' \
       $VAULT_ADDR/v1/sys/mounts/$NEW_ENGINE | jq
    set +x
}  || {
    exit 1
}

## Create a database connection
{
    set -x
    tee new_db_connection.json <<EOF
{
"plugin_name": "postgresql-database-plugin",
"allowed_roles": "*",
"connection_url": "postgresql://postgres:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
}
EOF

    curl --header "X-Vault-Token: ${VAULT_TOKEN}" \
       --request POST --data @new_db_connection.json \
       $VAULT_ADDR/v1/$NEW_ENGINE/config/$DB_NAME | jq
    set +x
}  || {
    exit 1
}

