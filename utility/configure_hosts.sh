#!/usr/bin/env bash

## Overwrite the hosts variable in env files
## Read parameters between ### DEPLOYMENT PARAMETERS ### and ###
##
## Instructions: 
##    1. Update the preferred hosts in config/ebs_preferred_hosts
##       Ex:
##         CB_HOST=cb.ebsproject.org
##         CBAPI_HOST=cbapi.ebsproject.org
##    2. Run the script
##          bash configure_hosts.sh

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Updating hosts... $RESET"
echo;

## Retrieve preferred hosts file
preferred_hosts_file="../config/ebs_preferred_hosts"

## Go to components directory
cd ../components

## Loop in the directory
shopt -s dotglob
find * -prune -type d | while IFS= read -r d; do 
    echo;
    echo "$BLUE $d $RESET"

    HOST_STR="_HOST"

    ## Read env file in the current component
    filename="$d/env"

    readarray -t lines <<< "$(sed -n '/^### DEPLOYMENT PARAMETERS ###.*/,/^###.*/p' "$filename")"

    for line in "${lines[@]}"; do

        ## Skip lines
        [[ "$line" == "### DEPLOYMENT PARAMETERS ###" ]] && [ -z "$line" ] && [[ "$line" =~ ^#.*$ ]] && continue

        ## Stop reading the file
        [[ "$line" = ^#.*$ ]] && exit

        ## Check if lines has *_HOST
        if [ -z "${line##*$HOST_STR*}" ] ;then
            while read -r host; do
                ## Set variables
                ORIGINAL_HOST_VARIABLE=${line%=*}
                ORIGINAL_HOST_VALUE=$(echo $line | cut -d "=" -f2)
                PREFERRED_HOST_VARIABLE=${host%=*}
                PREFERRED_HOST_VALUE=$(echo $host | cut -d "=" -f2)

                ## Check if the original variable in env is the same with variable in preferred
                if [[ "$ORIGINAL_HOST_VARIABLE" == "$PREFERRED_HOST_VARIABLE" ]] ;then                
                    {
                        echo "Updating host: $ORIGINAL_HOST_VARIABLE=$PREFERRED_HOST_VALUE"
                        ## Update the host variable
                        sed -i "s/$ORIGINAL_HOST_VALUE/$PREFERRED_HOST_VALUE/" $filename && {
                            echo;
                            echo "$GREEN ☑  DONE. $RESET"
                            echo;
                        }
                    } || {
                        echo;
                        echo "$RED ☒  Error in updating host. $RESET"
                        echo;
                        exit 1
                    }
                fi
            done < "$preferred_hosts_file"
        fi
    done 
done

