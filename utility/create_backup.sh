#!/bin/bash

### This script creates a backup of the EBS Deployment Tool directory of a cluster
###
### Instructions:
###     1. Run `bash create_backup.sh $1 $2 "$3"`
###     2. $1 is the name of the directory to be backed up. If there is no argument passed, it will backup the EBS Deployment Tool.
###     3. $2 is the filename of the backup file. If there is no argument passed, archived_ebs_deployment_tool_{current_date}.tar.gz will be used.
###     4. $3 is the name of the folders/files to be excluded in the backup. It should be enclosed in double quotes ("").
###        EXAMPLE: 
###                bash create_backup.sh
###                bash create_backup.sh /home/etenorio/projects/deployment-tool/components/
###                bash create_backup.sh /home/etenorio/projects/deployment-tool/components/ test.tar.gz
###                bash create_backup.sh /home/etenorio/projects/deployment-tool/components/ test.tar.gz "../archived/;../assets/"
###

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

## Set the directory to be backed up
if [[ "$1" != "" ]]; then
    directory="$1"
else
    ## Set the directory to EBS Deployment Tool
    directory="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && cd .. && pwd )"
fi
echo "Directory to be backed up: $directory"

## Set where to put the backup file and its name
if [[ "$2" != "" ]]; then
    filename="$2"
else
    current_directory="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
    filename="$current_directory/$(date '+%Y%m%d')_EBS_DEPLOYMENT_TOOL_BACKUP.tar.gz"
fi
echo "Backup file: $filename"

exclude_string_commands=""
## Check if there are files or directory to be excluded
if [[ "$3" != "" ]]; then
    echo "Folders/files to be excluded: $3"
    exclude_arr=$(echo "$3" | tr ";" "\n")
    for exclude in $exclude_arr
    do
        exclude_string_commands+=" --exclude='[$exclude]' "
    done
fi

{
    echo "$BLUE ---------------------------------------------------- $RESET"
    echo "$BLUE Creating a backup of the EBS Deployment Tool directory.. $RESET"
    echo;

    ## Preview
    echo "$BLUE $filename$RESET will be created as a backup of $BLUE$directory$RESET."
    echo;
    
    ## Create the tar.gz file
    tar -czvf $filename $directory $exclude_string_commands

    pwd=`pwd`

    echo; 
    echo "$GREEN ---------------------------------------------------- $RESET"
    echo "$GREEN $filename$RESET is created as a backup of $GREEN$directory$RESET."
    echo; 
    echo "$GREEN ☑  DONE. $RESET"
    echo;
} || {
    echo;
    echo "$RED ☒  Error in creating a backup of the EBS Deployment Tool directory $RESET"
    echo;
    exit 1
}