#!/bin/bash

do_help() {
  cat << EOF
Usage:

  $0 [-h] [-b <git branch name>] [PREVIOUS_RELEASE_TAG] [NEXT_RELEASE_TAG]

Description:

  Generate release notes based on git commit messages for EBS Breeding Analytics.

  Updates repositories and filters commits for release notes documentation.
    repositories: ba-core, ba-worker, ba-db-v2, ba-api, ba-ui
  Assumes deployment-tool is in the same-level directory as aforementioned repositories.

  -h  Display help
  -b  Specify branch name to check out

Sample usage:

  $0                      # Generate release notes since last remote tag up to present date
  $0 24.05.29             # Generate release notes since 24.05.29 up to present date
  $0 24.05.29 24.06.07    # Generate release notes from 24.05.29 to 24.06.07
  $0 -b release/24.09.25  # Generate release notes on release/24.09.25 branch
EOF
  exit 0
}

do_error() {
  echo "$1"
  exit 1
}

set_release() {
  if [ $# -eq 0 ]; then
    LAST_RELEASE=$(git describe --tags --abbrev=1)
    NEXT_RELEASE="HEAD"
    RELEASE_NAME="$LAST_RELEASE to $(date +%y.%m.%d)"
  elif [ $# -eq 1 ]; then
    LAST_RELEASE="$1"
    NEXT_RELEASE="HEAD"
    RELEASE_NAME="$LAST_RELEASE to $(date +%y.%m.%d)"
  elif [ $# -eq 2 ]; then
    LAST_RELEASE="$1"
    NEXT_RELEASE="$2"
    RELEASE_NAME="$LAST_RELEASE to $NEXT_RELEASE"
  else
    echo "Usage: ./ba_release_notes.sh [PREVIOUS_RELEASE_TAG] [NEXT_RELEASE_TAG]"
    exit 1
  fi
}

### Set Colors
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
PURPLE=$(tput setaf 5)
MINT=$(tput setaf 6)
RESET=$(tput sgr0)

### Other variables
PARENT_DIR=$(cd ../../ && pwd)
BA_CORE_FQN=$PARENT_DIR/ba-core
BA_WORKER_FQN=$PARENT_DIR/ba-worker
BA_DB_FQN=$PARENT_DIR/ba-db-v2
BA_API_FQN=$PARENT_DIR/ba-api
BA_UI_FQN=$PARENT_DIR/ba-ui

do_checkout() {
  BRANCH=$1
  if [[ -z "$BRANCH" ]]; then
    BRANCH=develop
  elif [[ -n "$BRANCH" ]]; then
    echo "$BRANCH"
  fi

  echo;
  echo "$BLUE Pulling latest $BRANCH branches... $RESET"
  echo;

  echo "$BLUE [1/5] ba-core $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git checkout $BRANCH && git pull origin $BRANCH

  echo;
  echo "$BLUE [2/5] ba-worker $RESET"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git checkout $BRANCH && git pull origin $BRANCH

  echo;
  echo "$BLUE [3/5] ba-db-v2 $RESET"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git checkout $BRANCH && git pull origin $BRANCH

  echo;
  echo "$BLUE [4/5] ba-api $RESET"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git checkout $BRANCH && git pull origin $BRANCH

  echo;
  echo "$BLUE [5/5] ba-ui $RESET"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git checkout $BRANCH && git pull origin $BRANCH
}

do_print() {
  # Pins all releases based on ba-ui repository!
  set_release "$@"
  
  echo;
  echo "$BLUE Generating release notes... $RESET"
  echo;
  echo "$BLUE ======= $RELEASE_NAME RELEASE NOTES ======= $RESET"
  echo;
  
  echo "$BLUE [1/5] ANALYSIS REQUEST MANAGER $RESET"
  echo;
  
  echo "$GREEN > New features $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  echo git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  
  echo "$YELLOW > Improvements $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  
  echo "$RED > Bug/Hot fixes $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  
  echo "$PURPLE > Documentation $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  
  echo "$MINT > Tests $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sARM"
  
  
  echo;
  echo "$BLUE [2/5] MOLECULAR DATA ANALYSIS $RESET"
  echo;
  
  echo "$GREEN > New features $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  
  echo "$YELLOW > Improvements $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  
  echo "$RED > Bug/Hot fixes $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  
  echo "$PURPLE > Documentation $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  
  echo "$MINT > Tests $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sMDA"
  
  echo;
  echo "$BLUE [3/5] PHENOTYPIC DATA MANAGER $RESET"
  echo;
  
  echo "$GREEN > New features $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  
  echo "$YELLOW > Improvements  $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  
  echo "$RED > Bug/Hot fixes $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  
  
  echo "$PURPLE > Documentation $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  
  echo "$MINT > Tests $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sPDM"
  
  
  echo;
  echo "$BLUE [4/5] STATISTICAL DESIGN MODELS $RESET"
  echo;
  
  echo "$GREEN > New features $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  
  echo "$YELLOW > Improvements $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  
  echo "$RED > Bug/Hot fixes $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  
  echo "$PURPLE > Documentation $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  
  echo "$MINT > Tests $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sSDM"
  
  
  echo;
  echo "$BLUE [5/5] BREEDING ANALYTICS PLATFORM $RESET"
  echo;
  
  echo "$GREEN > New features $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FEATURE\](\[IMPROVEMENT]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  
  echo "$YELLOW > Improvements $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[IMPROVEMENT\](\[FEATURE]|\[FIX\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  
  echo "$RED > Bug/Hot fixes $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\[FIX\](\[FEATURE]|\[IMPROVEMENT\]|\[DOC\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  
  echo "$PURPLE > Documentation $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\[DOC\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[TEST\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  
  echo "$MINT > Tests $RESET"
  cd "$BA_CORE_FQN" || (echo "ba-core repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_WORKER_FQN" || (echo "ba-worker repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_DB_FQN" || (echo "ba-db-v2 repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_API_FQN" || (echo "ba-api repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  cd "$BA_UI_FQN" || (echo "ba-ui repository not found" && exit)
  git log --pretty="%s" "$LAST_RELEASE".."$NEXT_RELEASE" | grep -i -E "^(\[FEATURE\]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\[TEST\](\[FEATURE]|\[IMPROVEMENT\]|\[FIX\]|\[DOC\]|\[REFACTOR\])*\sBDS\-[0-9]{1,6}\sBAP"
  
  
  echo;
  echo "$BLUE ======= END ======= $RESET"
  echo;
  
  exit 0
}

### Get user options
while getopts "b:h" opt; do
  case "$opt" in
    b) branch=$OPTARG;;
    h) do_help;;
    *) do_error "See $0 -h for options";;
  esac
done
shift $((OPTIND-1))

### Execute script
do_checkout "$branch"
do_print "$@"