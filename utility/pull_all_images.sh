#!/usr/bin/env bash

## Pull all Docker images listed  in an env file
## Read all the env files inside the components folder

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting loading Docker images... $RESET"
echo;

## Go to components directory
cd ../components

## Loop in the directory
shopt -s dotglob
find * -prune -type d | while IFS= read -r d; do 
    echo;
    echo "$BLUE $d $RESET"
    echo;
    {
        bash ../utility/pull_images.sh $d/env && {
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
        }
    } || {
        echo;
        echo "$RED ☒  Error in loading Docker images. $RESET"
        echo;
        exit 1
    }
done

