#!/bin/bash

### This script updates the Docker image tags in the env file 
### of a component to its previous version.
###
### Instructions:
###     1. Check the table below to determine the correct abbreviation.
###        Note: VERSION=1: previous_version (1 version before the current version)
###              VERSION=2: earlier_version (2 versions before the current version)
###
###     2. Run `bash rollback_services.sh {COMPONENT_ABBREVIATION} {COMPONENT_SERVICE} {VERSION}`
###        EXAMPLE:
###                bash rollback_services.sh ba arm-ui 2
###                bash rollback_services.sh cs cs-ebs-ui-proxy 1
###
## ------------------------------------------------------------------------------------------------------------------
## |            COMPONENT           |     ABBREVIATION     |                        SERVICES                        |
## ------------------------------------------------------------------------------------------------------------------
## |   Analytics Framework          |          af          |                   af-db, af-api, af-aeo                |
## |   Breeding Analytics           |          ba          |            ba-api, ba-arm, ba-db, ba-worker            |
## |   Breeding Station Operations  |          bso         |                        bso-api                         |
## |   Core Breeding                |          cb          |            cb-api, cb-db, cb-ui, cb-workers            |
## |   Core System                  |          cs          |        cs-api, cs-db, cs-ui, cs-printout-server,       |
## |                                |                      | cs-ebs-ui-proxy, cs-ebs-styleguide, cs-ebs-components, | 
## |                                |                      |        cs-ebs-shared-dependencies, cs-ebs-layout,      | 
## |                                |                      |                cs-printout, cs-shipment-tool           |
## |   Coruscant                    |       coruscant      |            traefik, rabbitmq, error-page-service       |
## |   GOBii GDM                    |         gobii        |        gobii-db, gobii-process, gobii_timescope        |
## |   Service Management           |          sm          |                    sm-api, sm-db, sm-ui                |
## ------------------------------------------------------------------------------------------------------------------
###

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

PWD=$(pwd)
PWD=$(echo $PWD | sed 's/deployment-tool.*/deployment-tool/g')

cd $PWD

MANIFEST_FILE="manifest.json"
LIST_FILE="utility/docker_images_directory.txt"

ENV_STR="_ENV"
ENV_FILE=""

COMPONENT_ARG=$1
SERVICE_ARG=$2
VERSION_ARG=$3

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]
then
    set +x
    echo;
    echo "$RED ☒  Invalid command. Please see instructions indicated in the script.. $RESET"
    exit;
fi

COMPONENT_ARG=$(echo $COMPONENT_ARG | awk '{print toupper($0)}')

COMPONENT=$(jq -r .Components.$COMPONENT_ARG $MANIFEST_FILE)
# checks if output is json or got false/null
if ! jq -e . >/dev/null 2>&1 <<<"$COMPONENT"; then
    echo "$RED ☒  Invalid command. Check if component input is valid. Please see instructions indicated in the script.. $RESET"
    exit;
fi

SERVICE=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\" $MANIFEST_FILE)
# checks if output is json or got false/null
if ! jq -e . >/dev/null 2>&1 <<<"$SERVICE"; then
    echo "$RED ☒  Invalid command. Check if service input is valid. Please see instructions indicated in the script.. $RESET"
    exit;
fi

IMAGE=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\".image $MANIFEST_FILE)

# checks if VERSION=1 OR VERSION=2
if [ $VERSION_ARG -eq 1 ]; then
    VERSION="previous_version"
elif [ $VERSION_ARG -eq 2 ]; then
    VERSION_ARG=0
    VERSION="earlier_version"
else 
    echo "$RED ☒  Invalid command. Check if version input is valid. Please see instructions indicated in the script.. $RESET"
    exit;
fi

TAG=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[$VERSION_ARG].$VERSION $MANIFEST_FILE)
# send error if tag retrieved from manifest file is an empty string // uncomment if needed
if [[ ${TAG} == "" ]]; then
    echo "$RED ☒  Rollback failed. Please see manifest file if populated.. $RESET"
    exit;
fi

# search for env file directory in docker_images_directory.txt
while read -r line; do
    
    ## Skip lines
    [ -z "${line##*}" ] && [ -z "$line" ] && [[ "$line" =~ ^\#.*$ ]] && continue

    ## Stop reading the file
    [[ "$line" = ^\#.*$ ]] && exit

    if [[ "$line" == *"$COMPONENT_ARG"* ]] ;then
        if [[ "$line" == *"$ENV_STR"* ]] ;then
            ENV_FILE=$(echo $line | cut -d "=" -f2)
        fi
    fi
done < "$LIST_FILE"

cd utility

# rollback to previous tag in corresponding env file
while read -r line; do

    ## Skip lines
    [ -z "${line##*}" ] && [ -z "$line" ] && [[ "$line" =~ ^\#.*$ ]] && continue

    ## Stop reading the file
    [[ "$line" = ^\#.*$ ]] && exit

    if [[ "$line" == *"$IMAGE"* ]] ;then
        DOCKER_IMAGE_VARIABLE=$(echo $line | cut -d "=" -f1)
        DOCKER_IMAGE_TAG_VARIABLE=$(echo ${DOCKER_IMAGE_VARIABLE#"CORUSCANT_"}_TAG)

        PREV_TAG=$(sed -n "/${DOCKER_IMAGE_TAG_VARIABLE}=/p" $ENV_FILE)
        PREV_TAG=$(echo $PREV_TAG | cut -d "=" -f2)

        echo "$BLUE Updating $IMAGE from $PREV_TAG to $TAG... $RESET"
        echo;
        
        sed -i -e "s/${DOCKER_IMAGE_TAG_VARIABLE}=.*/${DOCKER_IMAGE_TAG_VARIABLE}=${TAG}/" $ENV_FILE
        echo "$GREEN ☑  $IMAGE has been rolled back. $RESET"
    fi

done < "$ENV_FILE"

cd ../

# rollback manifest file
# previous_version to current_version
PREV_TAG=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[1].previous_version $MANIFEST_FILE)
PREV_DTS=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[1].deploymentTimestamp $MANIFEST_FILE)
cat <<< $(jq -r --arg CURR_VERSION $PREV_TAG ".Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[2].current_version=\$CURR_VERSION" $MANIFEST_FILE) > $MANIFEST_FILE
cat <<< $(jq -r --arg CURR_DTS "$PREV_DTS" ".Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[2].deploymentTimestamp=\$CURR_DTS" $MANIFEST_FILE) > $MANIFEST_FILE

# earlier_version to previous_version
EARLIER_TAG=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[0].earlier_version $MANIFEST_FILE)
EARLIER_DTS=$(jq -r .Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[0].deploymentTimestamp $MANIFEST_FILE)
cat <<< $(jq -r --arg PREV_VERSION $EARLIER_TAG ".Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[1].previous_version=\$PREV_VERSION" $MANIFEST_FILE) > $MANIFEST_FILE
cat <<< $(jq -r --arg PREV_DTS "$EARLIER_DTS" ".Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[1].deploymentTimestamp=\$PREV_DTS" $MANIFEST_FILE) > $MANIFEST_FILE

# empty out earlier_version
cat <<< $(jq -r --arg EMPTY_STR "" ".Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[0].earlier_version=\$EMPTY_STR" $MANIFEST_FILE) > $MANIFEST_FILE
cat <<< $(jq -r --arg EMPTY_STR "" ".Components.$COMPONENT_ARG.\"$SERVICE_ARG\".tags[0].deploymentTimestamp=\$EMPTY_STR" $MANIFEST_FILE) > $MANIFEST_FILE

echo;
echo "$GREEN ☑  Manifest file has been rolled back. $RESET"
echo;

DEPLOY_SCRIPT_PATH=$(echo "$ENV_FILE" | sed "s/env//")
DEPLOY_SCRIPT_PATH=$(echo "$DEPLOY_SCRIPT_PATH" | sed "s/..\///")

cd $DEPLOY_SCRIPT_PATH
bash deploy_stack.sh