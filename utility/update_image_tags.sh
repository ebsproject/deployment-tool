#!/bin/bash

### This script provides a quick way to update Docker image tags to the latest tag.
###
###     - Update Docker images listed in an env file
###     - Docker image must have variable name with *_IMAGE
###     - Docker image tag must have variable name with *_IMAGE_TAG
###
### Instructions:
###     1. Check the table below to determine the correct abbreviation.
###     2. Run `bash update_image_tags {COMPONENT_ABBREVIATION}`
###        EXAMPLE:
###                bash update_image_tags.sh ba
###                bash update_image_tags.sh ba af sm cs
###     3. If you want to update all env files, run `bash update_image_tags`
###        EXAMPLE: bash update_image_tags.sh
###
## ---------------------------------------------------------
## |            COMPONENT           |     ABBREVIATION     |
## ---------------------------------------------------------
## |   Analytics Framework          |          af          |
## |   Breeding Analytics           |          ba          |
## |   Breeding Station Operations  |          bso         |
## |   Core Breeding                |          cb          |
## |   Core System                  |          cs          |
## |   Coruscant                    |       coruscant      |
## |   GOBii GDM                    |         gobii        |
## |   Service Management           |          sm          |
## ---------------------------------------------------------
###

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

LIST_FILE="docker_images_directory.txt"

ENV_STR="_ENV"
DOCKER_IMAGE_STR="_IMAGE="

DOCKER_IMAGE=""
DOCKER_IMAGE_VARIABLE=""
DOCKER_IMAGE_TAG_VARIABLE=""
ENV_FILE=""
LATEST_TAG=""

ARG_ARRAY=()

if [ $# -eq 0 ]; then
    # Update all domains
    ARG_ARRAY=(AF BA BSO CB CS CORUSCANT GOBII SM)

    echo "$BLUE ------------------------------------------------------------------------------------------------- $RESET"
    echo "$BLUE Start updating env files of ALL domains... $RESET"
    echo "$BLUE ------------------------------------------------------------------------------------------------- $RESET"
else
    # Update domains in user input
    for ARG in "$@"
    do
        ARG_ARRAY+=("$ARG")
    done
fi


for ARG in "${ARG_ARRAY[@]}"
do
    COMPONENT=${ARG^^}
    echo;
    echo "$BLUE ------------------------------------------------------------------------------------------------- $RESET"
    echo "$BLUE Start updating env file of the following domain: $COMPONENT... $RESET"
    echo;

    while read -r line; do
        ## Skip lines
        [ -z "${line##*}" ] && [ -z "$line" ] && [[ "$line" =~ ^#.*$ ]] && continue

        ## Stop reading the file
        [[ "$line" = ^#.*$ ]] && exit

        ## Check lines under COMPONENT
        if [[ "$line" == *"$COMPONENT"* ]] ;then
            if [[ "$line" == *"$ENV_STR"* ]] ;then
                ENV_FILE=$(echo $line | cut -d "=" -f2)
            fi

            if [[ "$line" == *"$DOCKER_IMAGE_STR"* ]] ;then

                DOCKER_IMAGE_VARIABLE=$(echo $line | cut -d "=" -f1)
                DOCKER_IMAGE=$(echo $line | cut -d "=" -f2)

                LATEST_TAG=$(wget -q https://registry.hub.docker.com/v1/repositories/${DOCKER_IMAGE}/tags -O -  | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' | tr '}' '\n'  | awk -F: '{print $3}' | sed -e '/test/d' -e '/-RC/d' -e '/[a-z]/d' | awk 'END{print}')

                DOCKER_IMAGE_TAG_VARIABLE=$(echo ${DOCKER_IMAGE_VARIABLE#"CORUSCANT_"}_TAG)

                # Update Docker tag if there is a release tag available
                if [[ ${#LATEST_TAG} -ne 0 ]] ;then
                    PREV_TAG=$(sed -n "/${DOCKER_IMAGE_TAG_VARIABLE}=/p" $ENV_FILE)
                    PREV_TAG=$(echo $PREV_TAG | cut -d "=" -f2)

                    echo "$BLUE     Updating $DOCKER_IMAGE from $PREV_TAG to $LATEST_TAG... $RESET"

                    sed -i -e "s/${DOCKER_IMAGE_TAG_VARIABLE}=.*/${DOCKER_IMAGE_TAG_VARIABLE}=${LATEST_TAG}/" $ENV_FILE
                fi
            fi

            if [[ ${DOCKER_IMAGE} != "" ]] && [[ ${DOCKER_IMAGE_VARIABLE} != "" ]] && [[ ${DOCKER_IMAGE_TAG_VARIABLE} != "" ]] && [[ ${LATEST_TAG} != "" ]]; then
                ## Set the variables to empty
                DOCKER_IMAGE=""
                DOCKER_IMAGE_VARIABLE=""
                DOCKER_IMAGE_TAG_VARIABLE=""
                LATEST_TAG=""
            fi
        fi
    done < "$LIST_FILE"

    echo;
    echo "$GREEN Updating env file of $COMPONENT, DONE. $RESET"
    echo "$BLUE ------------------------------------------------------------------------------------------------- $RESET"
done

echo;