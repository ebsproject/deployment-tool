#!/bin/bash

### Provides a quick way to remove all services of a domain. Useful for
### performing management tasks involving only a specific EBS domain.
###
### Instructions:
###   1. Run `docker stack ls` to confirm the DOCKER STACK NAME you want to work with.
###   2. Check the table below to determine the correct abbreviation.
###   3. Run `bash undeploy_services {DOCKER STACK NAME} {COMPONENT ABBREVIATION}`
###      EXAMPLE: bash undeploy_services ebs ba
###
## -----------------------------------------------------------
## |      COMPONENT                 |        ABBREVIATION    |
## -----------------------------------------------------------
## |   Analytics Framework          |            af          |
## |   Breeding Analytics           |            ba          |
## |   Breeding Station Operations  |            bso         |
## |   Core Breeding                |            cb          |
## |   Core System                  |            cs          |
## |   GOBii GDM                    |            gobii       |
## |   Service Management           |            sm          |
## -----------------------------------------------------------
###

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

if [ -z "$1" ] || [ -z "$2" ]
then
    set +x
    echo;
    echo "$RED ☒  Invalid command. Please see instructions indicated in the script.. $RESET"
    exit;
fi

echo;
echo "$BLUE ☑ [1/3] Searching for for $1_$2* services... $RESET"

DOCKER_SERVICES=$(docker service ls | grep $1_$2 |  awk '{print "  ",$2}')

if [ -z "$DOCKER_SERVICES" ]
then
    set +x
    echo;
    echo "$RED ☒ No services to terminate. Please run the script again. $RESET"
    echo;
else
    echo;
    echo "$BLUE ☑ [2/3] Removing the following services ... $RESET"
    echo;
    echo "$DOCKER_SERVICES"
    echo;

    # Remove all services that match the provided paramaters
    SERVICES=$(docker service rm $(docker service ls | grep $1_$2 |  awk "{print \$1}"))

    echo;
    echo "$BLUE ☑ [3/3] Services have been removed. $RESET"
    echo;
    echo "$GREEN ☑  DONE. $RESET"
fi