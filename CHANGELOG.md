# 23.09.27...23.09.28 (2023-09-27)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ce58172a0b24757b47cfa191738ac88d96268cc4)

### New Features
    
- Add BA_DB_V2_HOST in db.env

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  

- Hotfix/[DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353)
  

- Release/23.04.29
  

- Create release/23.05.05
  

- [DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable
  

- Release/23.05.26
  

- Release/23.05.31
  

- Release/23.06.02
  

- Update SM env
  

- Update CS PS
  

- Update ENDPOINT_CB_GRAPHQL variable
  

- Release/23.06.16
  

- Release/23.06.23
  

- Release/23.06.29
  

- Release/23.07.01
  

- Create release/23.07.03
  

- Release/23.07.07
  

- Release/23.07.20
  

- Update BA DB V2
  

- Release/23.07.26
  

- Create release/23.07.27
  

- Update value of ENDPOINT_CS_SG
  

- Update value of ENDPOINT_CS_SG
  

- Update CS API
  

- Create release/23.08.04
  

- Update CB DB image tag
  

- Release/23.08.11
  

- Release/23.08.31
  

- Release/23.09.15
  

- Create release/23.09.22
  


# 23.09.22...23.09.27 (2023-09-27)


### Uncategorized
    
- Create release/23.09.22
  

- BA-2188 Add script to extract and generate release notes for Breeding Analytics
  


# 23.09.15...23.09.22 (2023-09-15)

- [[DEVOPS-2885](https://ebsproject.atlassian.net/browse/DEVOPS-2885) Add variables in SM API and File API](https://bitbucket.org/ebsproject/deployment-tool/commit/6a8f17973d83f793408a469b8a2d535056d1f179)

### New Features
    
- Add spring_servlet_multipart_max_request_size and spring_servlet_multipart_max_file_size

### Uncategorized
    
- [DEVOPS-2885](https://ebsproject.atlassian.net/browse/DEVOPS-2885) Add variables in SM API and File API
  

- Release/23.09.15
  


# 23.08.31...23.09.15 (2023-09-14)


### Uncategorized
    
- Release/23.08.31
  

- CORB-6242 Remove CS and BA URL env variables
  


# 23.08.11...23.08.31 (2023-08-31)


### Uncategorized
    
- [DEVOPS-2792](https://ebsproject.atlassian.net/browse/DEVOPS-2792) Update the health check for File API
  

- Release/23.08.11
  


# 23.08.04...23.08.11 (2023-08-07)


### Uncategorized
    
- Create release/23.08.04
  

- Update CB DB image tag
  


# 23.07.27...23.08.04 (2023-07-31)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ce58172a0b24757b47cfa191738ac88d96268cc4)

### New Features
    
- Add BA_DB_V2_HOST in db.env

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  

- Hotfix/[DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353)
  

- Release/23.04.29
  

- Create release/23.05.05
  

- [DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable
  

- Release/23.05.26
  

- Release/23.05.31
  

- Release/23.06.02
  

- Update SM env
  

- Update CS PS
  

- Update ENDPOINT_CB_GRAPHQL variable
  

- Release/23.06.16
  

- Release/23.06.23
  

- Release/23.06.29
  

- Release/23.07.01
  

- Create release/23.07.03
  

- Release/23.07.07
  

- Release/23.07.20
  

- Update BA DB V2
  

- Release/23.07.26
  

- Create release/23.07.27
  

- Update value of ENDPOINT_CS_SG
  

- Update value of ENDPOINT_CS_SG
  

- Update CS API
  




# 23.07.20...23.07.26 (2023-07-25)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ce58172a0b24757b47cfa191738ac88d96268cc4)
- [[DEVOPS-2709](https://ebsproject.atlassian.net/browse/DEVOPS-2709) Update CS stack](https://bitbucket.org/ebsproject/deployment-tool/commit/f14d38a5e9f34ff312d2db49b2f58f131c63f21d)
- [CORB-5853 Update CB Makefile](https://bitbucket.org/ebsproject/deployment-tool/commit/bdfd76afaa243d04d5efd4674116a9b8a85a6d55)

### New Features
    
- Add BA_DB_V2_HOST in db.env

- Add ebs_cb_graphql_endpoint

- test-watch-cb-api target

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  

- Hotfix/[DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353)
  

- Release/23.04.29
  

- Create release/23.05.05
  

- [DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable
  

- Release/23.05.26
  

- Release/23.05.31
  

- Release/23.06.02
  

- Update SM env
  

- Update CS PS
  

- Update ENDPOINT_CB_GRAPHQL variable
  

- Release/23.06.16
  

- Release/23.06.23
  

- Release/23.06.29
  

- Release/23.07.01
  

- Create release/23.07.03
  

- Release/23.07.07
  

- [DEVOPS-2709](https://ebsproject.atlassian.net/browse/DEVOPS-2709) Update CS stack
  

- CORB-5853 Update CB makefile
  


# 23.07.14...23.07.20 (2023-07-21)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ce58172a0b24757b47cfa191738ac88d96268cc4)

### New Features
    
- Add BA_DB_V2_HOST in db.env

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  

- Hotfix/[DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353)
  

- Release/23.04.29
  

- Create release/23.05.05
  

- [DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable
  

- Release/23.05.26
  

- Release/23.05.31
  

- Release/23.06.02
  

- Update SM env
  

- Update CS PS
  

- Update ENDPOINT_CB_GRAPHQL variable
  

- Release/23.06.16
  

- Release/23.06.23
  

- Release/23.06.29
  

- Release/23.07.01
  

- Create release/23.07.03
  

- Release/23.07.07
  

- BA-1956 Add SM-API and FILE-API env variables
  


# 23.07.07...23.07.14 (2023-07-06)

- [[DEVOPS-2654](https://ebsproject.atlassian.net/browse/DEVOPS-2654) Update CS stack](https://bitbucket.org/ebsproject/deployment-tool/commit/be8456a1dc9192670c37993ab1d4e41b53a1ccc0)

### New Features
    
- Update CS User Sync environment variables

### Uncategorized
    
- [DEVOPS-2654](https://ebsproject.atlassian.net/browse/DEVOPS-2654) Update CS stack
  




# 23.07.01...23.07.03 (2023-06-30)


### Uncategorized
    
- Add CS User Sync Docker Image
  




# 23.06.23...23.06.29 (2023-06-27)

- [[DEVOPS-2605](https://ebsproject.atlassian.net/browse/DEVOPS-2605) Add CS User Sync](https://bitbucket.org/ebsproject/deployment-tool/commit/edf2be51cf885484a8d4e907381942a58e6b3121)

### New Features
    
- Add CS User Sync in CS stack

### Uncategorized
    
- [DEVOPS-2569](https://ebsproject.atlassian.net/browse/DEVOPS-2569) Remove AWS-related environment variables from File API
  

- [DEVOPS-2605](https://ebsproject.atlassian.net/browse/DEVOPS-2605) Add CS User Sync
  


# 23.06.16...23.06.23 (2023-06-21)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ce58172a0b24757b47cfa191738ac88d96268cc4)
- [[DEVOPS-2578](https://ebsproject.atlassian.net/browse/DEVOPS-2578) Update SM API variables](https://bitbucket.org/ebsproject/deployment-tool/commit/02cb0e46669c3f1d0cd2b39ca291c20be2282190)

### New Features
    
- Add BA_DB_V2_HOST in db.env

- Add ebs_cb_graphql_endpoint

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  

- Hotfix/[DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353)
  

- Release/23.04.29
  

- Create release/23.05.05
  

- [DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable
  

- Release/23.05.26
  

- Release/23.05.31
  

- Release/23.06.02
  

- Update SM env
  

- Update CS PS
  

- Update ENDPOINT_CB_GRAPHQL variable
  

- [DEVOPS-2578](https://ebsproject.atlassian.net/browse/DEVOPS-2578) Update SM API variables
  




# 23.06.09...23.06.13 (2023-06-14)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ce58172a0b24757b47cfa191738ac88d96268cc4)
- [[DEVOPS-2421](https://ebsproject.atlassian.net/browse/DEVOPS-2421) Update CB service](https://bitbucket.org/ebsproject/deployment-tool/commit/bf96bed4b8bcb9df0d9c5e6f11b3bd8197d6b88f)

### New Features
    
- Add BA_DB_V2_HOST in db.env

- Remove command; Add env variables

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  

- Hotfix/[DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353)
  

- Release/23.04.29
  

- Create release/23.05.05
  

- [DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable
  

- Release/23.05.26
  

- Release/23.05.31
  

- Release/23.06.02
  

- Update SM env
  

- Update CS PS
  

- Feature/[DEVOPS-2421](https://ebsproject.atlassian.net/browse/DEVOPS-2421)
  

- BA-1754 Update BA Makefile
  




# 23.05.31...23.06.02 (2023-05-31)


### Uncategorized
    
- [DEVOPS-2389](https://ebsproject.atlassian.net/browse/DEVOPS-2389) Remove New Relic
  


# 23.05.26...23.05.31 (2023-05-29)


### Uncategorized
    
- BA-1757 Update BA stack with SQS environment variables
  


# 23.05.05...23.05.26 (2023-05-24)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2344](https://ebsproject.atlassian.net/browse/DEVOPS-2344) Update BA stack file](https://bitbucket.org/ebsproject/deployment-tool/commit/d2ee518a74327a6627e581574ad696e2a49c6907)
- [[DEVOPS-2296](https://ebsproject.atlassian.net/browse/DEVOPS-2296) Add utility script](https://bitbucket.org/ebsproject/deployment-tool/commit/c49dd14522f6c2d6c7d751098905c622f9c908c9)
- [[DEVOPS-2419](https://ebsproject.atlassian.net/browse/DEVOPS-2419) Add variable](https://bitbucket.org/ebsproject/deployment-tool/commit/50a23da0b73301180a2604cbfbbb9be4c55ead71)
- [[DEVOPS-2469](https://ebsproject.atlassian.net/browse/DEVOPS-2469) Update SM stack](https://bitbucket.org/ebsproject/deployment-tool/commit/79081420330668f215bb2977eaedddd1eb0d09af)

### New Features
    
- Add BA DB V2 service

- Create a script to validate the Docker images

- Add BA_DB_V2_HOST in db.env

- Add SM MarkerDB DB and API services

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- [DEVOPS-2344](https://ebsproject.atlassian.net/browse/DEVOPS-2344) Update BA stack file
  

- Update CB API Docker image
  

- [DEVOPS-2353](https://ebsproject.atlassian.net/browse/DEVOPS-2353) Update CB GraphQL API ports
  

- Feature/[DEVOPS-2296](https://ebsproject.atlassian.net/browse/DEVOPS-2296)
  

- Feature/BA-1729
  

- Feature/CORB-5586
  

- Feature/[DEVOPS-2469](https://ebsproject.atlassian.net/browse/DEVOPS-2469)
  

- Add /graphql
  

- [DEVOPS-2479](https://ebsproject.atlassian.net/browse/DEVOPS-2479) Update GIGWA_DB_PASSWORD default value
  




# 23.04.26...23.04.29 (2023-04-28)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Release/23.03.29
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  

- Create release/23.04.01
  

- Release/23.04.14
  

- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Release/23.04.21
  

- Update CB UI and API Docker images
  

- Update CB API Docker image
  


# 23.04.21...23.04.26 (2023-04-26)

- [[DEVOPS-2341](https://ebsproject.atlassian.net/browse/DEVOPS-2341) Update CB and CS stack files](https://bitbucket.org/ebsproject/deployment-tool/commit/dd0e43a35139bcdb998ed14238212119f0661fbb)

### New Features
    
- Add CB GrapQL API service; Update CS PS variables

### Uncategorized
    
- [DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers
  

- Feature/[DEVOPS-2341](https://ebsproject.atlassian.net/browse/DEVOPS-2341)
  


# 23.04.01...23.04.21 (2023-04-14)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-2216](https://ebsproject.atlassian.net/browse/DEVOPS-2216) Update CB Workers](https://bitbucket.org/ebsproject/deployment-tool/commit/9450652d97ff04d2130a68d7e1ef4dfea9efc93a)

### New Features
    
- Add SG-related variables in CB Workers

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Add SMTP variables
  

- Add SMTP variables
  

- Update CS API variables
  


# 23.03.29...23.04.01 (2023-03-29)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Create release/23.02.15
  

- Update SM DB Docker image tag
  

- Update SM DB Docker image tag
  

- Add new line
  

- Update AF AEO Docker image tag
  

- Update AF AEO Docker image tag
  

- Create release/23.08.28
  

- Update BA env
  

- Update BA API v2 environment variable
  

- Release/23.03.17
  

- Add SMTP variables
  


# 23.03.17...23.03.29 (2023-03-29)


### Uncategorized
    
- BA-1594 Add AWS credentials
  

- CORB-5266 Add tragets for Trivy
  


# 23.02.28...23.03.17 (2023-03-16)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  

- Release/23.02.01
  

- [DEVOPS-1948](https://ebsproject.atlassian.net/browse/DEVOPS-1948) Remove CB_API_SWARM_URL from ba-stack.yml and the env file under BA
  

- Release/23.02.10
  

- Create release/23.02.11
  

- Update SM DB Docker image tag
  

- Updated AF AEO Docker image tag from dev to dev-ecs-23.02.13
  

- Update Gigwa variables
  

- Add a new line to fix script in DO Swarm Deployer
  

- Update AF AEO Docker image tag
  

- Feature/BA-1472
  

- [DEVOPS-2175](https://ebsproject.atlassian.net/browse/DEVOPS-2175) Add SM API variables
  

- BA-1472: Add AF AEO & DB targets
  

- CORB-5146 CB Deployment Tool - Add environment variable for CB API component
  

- BA-1472 Update Makefile for Linux and Mac
  

- Update CB BrAPI redirect URI
  

- Update BA API v2 variables
  


# 23.02.15...23.02.28 (2023-02-22)


### Uncategorized
    
- Create release/23.02.11
  

- Update SM DB Docker image tag
  

- Update AF AEO Docker image tag
  


# 23.02.10...23.02.15 (2023-02-11)


### Uncategorized
    
- Release/23.02.01
  

- Release/23.02.10
  


# 23.02.01...23.02.10 (2023-02-01)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  

- Release/22.03.23
  

- Update CB Docker image tags
  

- Release/22.04.05
  

- Release/22.04.20
  

- Release/22.04.27
  

- Release/22.05.26
  

- Relase/22.06.02
  

- Updated CB API image tag
  

- Release/22.06.21
  

- Update CB API tag
  

- Release/22.06.29
  

- Update CB API
  

- Update CB API
  

- Update default value of SM_API_PROFILE
  

- Update default value of SM_API_PROFILE
  

- Update AF AEO image tag
  

- Generate 22.07.15
  

- Update CB UI and API image tags
  

- Update CB UI and API image tags
  

- Update CB and SM env
  

- Disable CS API healthcheck
  

- Update CB API and DB image tags
  

- Update CB API and DB image tags
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Update CB API image tag
  

- Update CB API image tag
  

- Update CB DB image tag
  

- Update CB DB image tag
  

- Release/22.07.15
  

- Update BA DB host in BA Worker
  

- Generate release/22.08.05
  

- Generate release/22.08.19
  

- Generate release/22.08.26
  

- Update CB image tag
  

- Generate release/22.08.31
  

- Update BA Worker image tag
  

- Update BA Worker image tag
  

- Update BA ARM
  

- Update BA ARM
  

- Fix BA DB Host
  

- Fix BA DB Host
  

- Create release/22.09.09
  

- Create release/22.09.14
  

- Update CB image tag
  

- Release/22.09.28
  

- Update CB image tag
  

- Update CB image tag
  

- Release/22.10.07
  

- Release/22.10.21
  

  

- Generate release/22.10.28
  

- Update CS image tags
  

- Update CS image tags
  

- Generate release/22.11.04
  

- Update the default value of CB_NODE_ENV
  

- Release/22.12.01
  

- Update CB env
  

- Release/22.12.01
  

- Release/22.12.15
  

- Release/23.01.20
  


# 23.01.20...23.02.01 (2023-02-01)


### Uncategorized
    
- BA-1381 Add CS and CB environment variables
  


# 22.12.15...23.01.20 (2023-01-20)

- [CORB-4633 Update CB Makefile](https://bitbucket.org/ebsproject/deployment-tool/commit/b3d6c1b9bbd8fd875032ad3a2843a06acbfb06dc)
- [CORB-4633 Update CB Makefile](https://bitbucket.org/ebsproject/deployment-tool/commit/3147167dd8626ac74b5969ea3f7649330a9d98e9)

### New Features
    
- Add new rules for CB-BrAPI unit test execution and reports

- Add a new rule for installing a new package in CB-BrAPI

### Uncategorized
    
- [DEVOPS-1844](https://ebsproject.atlassian.net/browse/DEVOPS-1844) Add env variable for previous path
  

- CORB-4699 Add BA base url in CB env file
  

- CORB-3289 Add ba-api url to env file
  

- Feature/CORB-4633
  


# 22.12.01...22.12.15 (2022-12-09)

- [[DEVOPS-1762](https://ebsproject.atlassian.net/browse/DEVOPS-1762) Update SM API variables](https://bitbucket.org/ebsproject/deployment-tool/commit/d1369c0dc06504957e27a3fc8cc14788613d8a48)
- [[DEVOPS-1734](https://ebsproject.atlassian.net/browse/DEVOPS-1734) Update CS API env variables](https://bitbucket.org/ebsproject/deployment-tool/commit/bfd14836fef2bac3193d2db5a1914b126a4a897b)

### New Features
    
- Update variables with dots with undescores

- Update the variables in CS API

### Uncategorized
    
- [DEVOPS-1762](https://ebsproject.atlassian.net/browse/DEVOPS-1762) Update SM API variables
  

- [DEVOPS-1734](https://ebsproject.atlassian.net/browse/DEVOPS-1734) Update CS API env variables
  


# 22.10.21...22.12.01 (2022-12-01)

- [[DEVOPS-1624](https://ebsproject.atlassian.net/browse/DEVOPS-1624) Update MFEs in CS and SM](https://bitbucket.org/ebsproject/deployment-tool/commit/8c49929d25afa8e5fd45247a704557ac0af650e4)
- [CORB-4634 Add cb-brapi initial deployment configurations](https://bitbucket.org/ebsproject/deployment-tool/commit/c20d1d5760ad9110f99a15c0ad76d844db801e85)
- [CORB-4634 Reset Makefile updates](https://bitbucket.org/ebsproject/deployment-tool/commit/09178d068059a9472de5dd5d5dcdfca17fbb1abd)
- [[DEVOPS-1675](https://ebsproject.atlassian.net/browse/DEVOPS-1675) Fix error in the service port.](https://bitbucket.org/ebsproject/deployment-tool/commit/1f6db0b896ba88fe6fca45b52c86e69e4a3994ad)
- [CORB-4634 Update CB BRAPI port and default tag](https://bitbucket.org/ebsproject/deployment-tool/commit/cf46948c5cdef230efdee0e5823c3d6402272b6b)
- [CORB-4634 Update CB BRAPI and CB API ports](https://bitbucket.org/ebsproject/deployment-tool/commit/b8752b014741061628b9972d79d97406998eab00)
- [[DEVOPS-1675](https://ebsproject.atlassian.net/browse/DEVOPS-1675) Update env variables for BA API.](https://bitbucket.org/ebsproject/deployment-tool/commit/e92798ec9dc25838bd46f04dd9bb7cc284e81dbf)
- [CORB-4634 Update CB CBRAPI redirect URI](https://bitbucket.org/ebsproject/deployment-tool/commit/a333bd0b05f4b8d63b74a378119ad4055e99f4c6)
- [CORB-4636 Add CBAPI_TOKEN_URL to env variables](https://bitbucket.org/ebsproject/deployment-tool/commit/84d38ba0348c6a94eaf12bdb0b9dc5e176163622)
- [Update manifest.json](https://bitbucket.org/ebsproject/deployment-tool/commit/aef8f7ac49bb7e16875adf5a5e3939d32eaeeec7)
- [CORB-4670 Update CB BRAPI labels in the stack file](https://bitbucket.org/ebsproject/deployment-tool/commit/4913b8ca57a2443d9da9e0ad44d6eaf2de0c8396)
- [[DEVOPS-1728](https://ebsproject.atlassian.net/browse/DEVOPS-1728) Add BA UI](https://bitbucket.org/ebsproject/deployment-tool/commit/7df674ae70d32fe066c81e2f33169c5fa73cd653)
- [[DEVOPS-1728](https://ebsproject.atlassian.net/browse/DEVOPS-1728) Update BA env](https://bitbucket.org/ebsproject/deployment-tool/commit/55c2b3882e6c7b54bc58ffcd87a4e27ca5dfd60e)
- [[DEVOPS-1728](https://ebsproject.atlassian.net/browse/DEVOPS-1728) Replace credentials with default values](https://bitbucket.org/ebsproject/deployment-tool/commit/198fe1e70fb4c314a1e5a2015bdf66be1b4910be)

### New Features
    
- Add Shipment Manager and Notification

- Add CB BRAPI environment parameters and new docker service

- Add CB BrAPI Makefile targets

- Revert init and restore-db recipe updates

- Add CB BrAPI

- Add BA UI in BA Stack file

- Update env variables in BA

- Update default credentials to xxxx

### Fixes
    
- Fix incorrect port in the service. The port should be 80, not 5000, as this is what is configured in the Docker image.

- Previous env variables were incorrect. The values now are based on the template in the config directory.

### Uncategorized
    
- Feature/[DEVOPS-1564](https://ebsproject.atlassian.net/browse/DEVOPS-1564)
  

- [DEVOPS-1624](https://ebsproject.atlassian.net/browse/DEVOPS-1624) Update MFEs in CS and SM
  

- [DEVOPS-1666](https://ebsproject.atlassian.net/browse/DEVOPS-1666) Updated the cs-api environment configuration in cs-stack.yml in the deployment tool
  

- feat: Replace port number 3001 with 3000 (same with CB API)
  feat: Replace default image tag 'test' to 'dev'

- feat: Use env variable for setting  CB BRAPI and CB API service ports
  fix: Remove CB BRAPI default ports in the stack file

- Add BA API (V2) to the BA Stack
  

- feature: Use https://cbbrapi.local/v2/auth/callback for CB BRAPI authentication callback

- fix: Revert previously omitted env variable: CBAPI_TOKEN_URL

- Feature/CORB-4634
  

- feat: Comment out the line for using traefik error page middleware to display the exact API error responses

- CORB-4670 Update CB BRAPI labels in the stack file
  

- Remove env variables that are not expected to regularly deviate from the default values in the Dockerfile.
  

- Update manifest.json
  

- [DEVOPS-1728](https://ebsproject.atlassian.net/browse/DEVOPS-1728) Add BA UI
  


# 22.07.15...22.10.21 (2022-10-19)

- [[DEVOPS-1331](https://ebsproject.atlassian.net/browse/DEVOPS-1331) Add Gigwa in SM stack file](https://bitbucket.org/ebsproject/deployment-tool/commit/3a62ece0bcabd3fb89648b4c539177d73efbfd9f)
- [[DEVOPS-1099](https://ebsproject.atlassian.net/browse/DEVOPS-1099) Remove PG_DRIVER env variable from all database services in the EBS Deployment Tool](https://bitbucket.org/ebsproject/deployment-tool/commit/6854cbdd59acbea276297887a7e30670a5cbeb13)
- [CORB-4023 Add CB_DATA_BROWSER_DEFAULT_PAGE_SIZE env variable for cb](https://bitbucket.org/ebsproject/deployment-tool/commit/9ab709820f629b81de6cd361f70f2a5738b99862)
- [[DEVOPS-1431](https://ebsproject.atlassian.net/browse/DEVOPS-1431) Add matomo service](https://bitbucket.org/ebsproject/deployment-tool/commit/ab32a0ebdab85177423ea716a36c554a94c75029)
- [[DEVOPS-674](https://ebsproject.atlassian.net/browse/DEVOPS-674) Add BA PDM API](https://bitbucket.org/ebsproject/deployment-tool/commit/422cf69763b632d183422d1bfdf005b350fc2cf6)
- [[DEVOPS-594](https://ebsproject.atlassian.net/browse/DEVOPS-594) Add file api](https://bitbucket.org/ebsproject/deployment-tool/commit/d6d9ca2999ed3634b076b3979df4998aaad14160)
- [[DEVOPS-594](https://ebsproject.atlassian.net/browse/DEVOPS-594) Update environment variables in File API](https://bitbucket.org/ebsproject/deployment-tool/commit/3cfab658bc30badf32271dec03904841242fe695)
- [[DEVOPS-136](https://ebsproject.atlassian.net/browse/DEVOPS-136) Remove unused DB environment variables](https://bitbucket.org/ebsproject/deployment-tool/commit/61ad86ce4144e89d7d06351d4347a95d4ff29169)
- [[DEVOPS-673](https://ebsproject.atlassian.net/browse/DEVOPS-673) Reorganize CS stack file](https://bitbucket.org/ebsproject/deployment-tool/commit/0cf0d915ee13e38dd94ed251813cc4651e3db0c9)
- [CORB-4213 Update CB env variables](https://bitbucket.org/ebsproject/deployment-tool/commit/2efc96ba13cde3c568277aeece89ed604f5bf3d1)
- [CORB-4213 Update CB API node env variable](https://bitbucket.org/ebsproject/deployment-tool/commit/ecec434da82a510495e8314289f52538e2cf12cc)
- [CORB-4213 Update CB env and stack files](https://bitbucket.org/ebsproject/deployment-tool/commit/6f16677aa987d1993b94b68ade9659d43e18d5c0)

### New Features
    
- Add Gigwa in SM stack in Deployment Tool

- Remove PG_DRIVER env variable from all database services in the EBS Deployment Tool. Add comment in  pg_driver=${PG_DRIVER}.

- Set the default page size across CB data browsers. Add new env variable

- Add mysql and matomo services in coruscant stack file

- ADD BA PDM API in BA Stack file

- Add File API service in Coruscant

- Update environment variables in File API

- Remove unused DB environment variables in the stack files (BA,CB,CS,SM)

- Remove Shared Dependencies; Remove healthcheck of MFEs; Add EBS Notification;

- Add CBAPI_NODE_ENV deployment parameter; Add CB_HOST and NODE_ENV CB stack env variables

- Move CBAPI_NODE_ENV var under the application config params

- Use CB_NODE_ENV variable for setting API and workers node environments

### Uncategorized
    
- [DEVOPS-1223](https://ebsproject.atlassian.net/browse/DEVOPS-1223) Add pg_stat_statements settings
  

- [DEVOPS-1229](https://ebsproject.atlassian.net/browse/DEVOPS-1229) Parameterize database host and port
  

- Update BA DB URL
  

- Update BA DB host in BA Worker
  

- Fix BA DB host
  

- [DEVOPS-1331](https://ebsproject.atlassian.net/browse/DEVOPS-1331) Add Gigwa in SM stack file
  

- CORB-4024 Data Browser: Add CB_DATA_BROWSER_MAX_PAGE_SIZE in cbconfig
  

- [DEVOPS-1099](https://ebsproject.atlassian.net/browse/DEVOPS-1099) Remove PG_DRIVER env variable from all database services in the EBS Deployment Tool
  

- CORB-4023 Add CB_DATA_BROWSER_DEFAULT_PAGE_SIZE env variable for cb
  

- Feature/CORB-4048
  

- [DEVOPS-1431](https://ebsproject.atlassian.net/browse/DEVOPS-1431) Add matomo service
  

- [DEVOPS-674](https://ebsproject.atlassian.net/browse/DEVOPS-674) Add BA PDM API
  

- [DEVOPS-594](https://ebsproject.atlassian.net/browse/DEVOPS-594) Add file api
  

- [DEVOPS-136](https://ebsproject.atlassian.net/browse/DEVOPS-136) Remove unused DB environment variables
  

- [DEVOPS-673](https://ebsproject.atlassian.net/browse/DEVOPS-673) Reorganize CS stack file
  

- [DEVOPS-594](https://ebsproject.atlassian.net/browse/DEVOPS-594) Update environment variables in File API
  

- CORB-4213 Update CB env variables
  

- CORB-4213 Update CB env and stack files
  

- CORB-4393 Update CB Makefile
  

- [DEVOPS-1510](https://ebsproject.atlassian.net/browse/DEVOPS-1510) Add Matomo environment variables to CB UI cbconfig
  

- Feature/[DEVOPS-1541](https://ebsproject.atlassian.net/browse/DEVOPS-1541)
  




# 22.06.21...22.06.29 (2022-06-29)


### Uncategorized
    
- [DEVOPS-1106](https://ebsproject.atlassian.net/browse/DEVOPS-1106) Add pg_timetable credentials
  

- CORB-3355 add r version in simba.conf
  

- [DEVOPS-1106](https://ebsproject.atlassian.net/browse/DEVOPS-1106) Change variables to uppercase
  

- CORB-3571 Update CB Makefile for Sprint 22.06.29
  


# release/22.06.02...22.06.21 (2022-06-10)

- [[DEVOPS-1038](https://ebsproject.atlassian.net/browse/DEVOPS-1038) Add cb-db environment variables](https://bitbucket.org/ebsproject/deployment-tool/commit/60b6259af6064d396e3a159b32e0ce69968ce9c1)
- [[DEVOPS-1053](https://ebsproject.atlassian.net/browse/DEVOPS-1053) Update SM API environment variables](https://bitbucket.org/ebsproject/deployment-tool/commit/96629f10a5c0403262123daa533bca071fab8e20)

### New Features
    
- Add ebs.cb.api.endpoint in sm-api

### Uncategorized
    
- Add max_connections = 800
  Add idle_in_transaction_session_timeout = 3min
  Edit liquibase contexts = schema, template
  Edit liquibase labels = NULL
  Trim trailing spaces upon save in VSCode

- [DEVOPS-1053](https://ebsproject.atlassian.net/browse/DEVOPS-1053) Update SM API environment variables
  

- Feature/[DEVOPS-1032](https://ebsproject.atlassian.net/browse/DEVOPS-1032)
  

- [DEVOPS-1038](https://ebsproject.atlassian.net/browse/DEVOPS-1038) Update parameters for liquibase upgrade
  


# 22.05.26...release/22.06.02 (2022-05-31)


### Uncategorized
    
- Add missing gobii-process folder
  


# 22.04.27...22.05.26 (2022-05-23)

- [[DEVOPS-969](https://ebsproject.atlassian.net/browse/DEVOPS-969) Update SM API configuration in the EBS Deployment Tool to include new environment variables.](https://bitbucket.org/ebsproject/deployment-tool/commit/765cb693308c32a7c9df4ca07c46fba71e201b1a)
- [[DEVOPS-970](https://ebsproject.atlassian.net/browse/DEVOPS-970) Setup storage persistence for printout backend file directory](https://bitbucket.org/ebsproject/deployment-tool/commit/d39e1c200c32f768f9a2631e40b441536ac41c1e)
- [[DEVOPS-1028](https://ebsproject.atlassian.net/browse/DEVOPS-1028) Update BA stack file](https://bitbucket.org/ebsproject/deployment-tool/commit/f8a4c2dc4a19ccc76646715ade1a3271f6d3d505)

### New Features
    
- Add CS_API_URL in SM stack file

- Add cs-volume/files

- Remove the arm-ui service from the BA stack file.

### Uncategorized
    
- Update sm-ui tags in manifest.json
  

- [DEVOPS-993](https://ebsproject.atlassian.net/browse/DEVOPS-993) Add CS Chart in CS stack and env file
  

- [DEVOPS-969](https://ebsproject.atlassian.net/browse/DEVOPS-969) Update SM API configuration in the EBS Deployment Tool to include new environment variables.
  

- [DEVOPS-970](https://ebsproject.atlassian.net/browse/DEVOPS-970) Setup storage persistence for printout backend file directory
  

- [DEVOPS-970](https://ebsproject.atlassian.net/browse/DEVOPS-970) Update directories for cs-ps volume
  

- [DEVOPS-1028](https://ebsproject.atlassian.net/browse/DEVOPS-1028) Update BA stack file
  

- [DEVOPS-1006](https://ebsproject.atlassian.net/browse/DEVOPS-1006) Remove the labels under SM UI in the SM stack file as well as cleanup the commented portion
  

- [DEVOPS-1007](https://ebsproject.atlassian.net/browse/DEVOPS-1007) Remove the labels on the BSO Stack file under BSO API
  


# 22.04.20...22.04.27 (2022-04-26)


### Uncategorized
    
- Match GDM API restart to CB-API restart
  

- CORB-1744 Deployment Tool - Add BSO_API_URL env variable
  


# 22.04.05...22.04.20 (2022-04-18)

- [Add config for EBS-GDM-API](https://bitbucket.org/ebsproject/deployment-tool/commit/f859bfa7b82ff51b50983cc382f9eb77283c6a92)
- [[DEVOPS-918](https://ebsproject.atlassian.net/browse/DEVOPS-918) Organize CB env files](https://bitbucket.org/ebsproject/deployment-tool/commit/7aa89257574d4ba1f0ce90e4c69e7885a502d2e2)
- [Modify gobii_gdm stack file](https://bitbucket.org/ebsproject/deployment-tool/commit/59fd43cd8d717dd20989dc106c4018c73e81c059)
- [[DEVOPS-921](https://ebsproject.atlassian.net/browse/DEVOPS-921) Move any shipment related activities from CS to SM](https://bitbucket.org/ebsproject/deployment-tool/commit/fd6e1fc7529bc5aa81cb03f5ed07bf34b128250d)

### New Features
    
- Adds ability to deploy EBS-GDM-API

- Remove unnecessary variables.

- Integrate traefik in API labels.

- Move shipment tool service from CS to SM stack file

### Uncategorized
    
- [DEVOPS-934](https://ebsproject.atlassian.net/browse/DEVOPS-934) Update tags format in manifest file
  

- Feature/[DEVOPS-904](https://ebsproject.atlassian.net/browse/DEVOPS-904)
  

- Add config for EBS-GDM-API
  

- [DEVOPS-918](https://ebsproject.atlassian.net/browse/DEVOPS-918) Organize CB env files
  

- [DEVOPS-921](https://ebsproject.atlassian.net/browse/DEVOPS-921) Move any shipment related activities from CS to SM
  

- [DEVOPS-921](https://ebsproject.atlassian.net/browse/DEVOPS-921) Add LIB_VERSION variable in SM env file
  


# 22.03.23...22.04.05 (2022-04-05)

- [CORB-2798 Update CB stack yml file](https://bitbucket.org/ebsproject/deployment-tool/commit/246c0f0cef03ff950e86e473d4aa556b577b8e77)
- [Revert "CORB-2798 Update CB stack yml file"](https://bitbucket.org/ebsproject/deployment-tool/commit/e4b5a953b28805bcef25197363fc446a70259d72)
- [CORB-2802 Update CB Makefile](https://bitbucket.org/ebsproject/deployment-tool/commit/26f1d27491498aa72949aa3718641fd853b00736)
- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)
- [[DEVOPS-845](https://ebsproject.atlassian.net/browse/DEVOPS-845) Create a manifest file](https://bitbucket.org/ebsproject/deployment-tool/commit/e8e2144888abcb506f49b648f2194260ff3d1587)
- [[DEVOPS-845](https://ebsproject.atlassian.net/browse/DEVOPS-845) Update format](https://bitbucket.org/ebsproject/deployment-tool/commit/957ec1ba3d53f0744165386583073c0f926d9b62)
- [[DEVOPS-905](https://ebsproject.atlassian.net/browse/DEVOPS-905) Update PostgreSQL authentication](https://bitbucket.org/ebsproject/deployment-tool/commit/8003669169f905fb85eebb80549ce83c576d67e2)
- [[DEVOPS-905](https://ebsproject.atlassian.net/browse/DEVOPS-905) Update badb user](https://bitbucket.org/ebsproject/deployment-tool/commit/71aeca4a9592a01f016da9b0d4fe332a15222a0b)

### New Features
    
- Add CBAPI_HOST to cb-api environment variables

- Add pause and unpause cb-api targets

- Create a manifest file that contains the list of Docker images to be deployed per component

- Update format of manifest.json

- Update dafault PostgreSQL authentication to md5

- Update BA DB user in AF and BA env files

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Feature/[DEVOPS-811](https://ebsproject.atlassian.net/browse/DEVOPS-811)
  

- Feature/CORB-2745
  

- CORB-2798 Update CB stack yml file
  

- This reverts commit 246c0f0cef03ff950e86e473d4aa556b577b8e77.

- Revert "CORB-2798 Update CB stack yml file"
  

- Fix AF API tag
  

- Feature/CORB-2802
  

- [DEVOPS-856](https://ebsproject.atlassian.net/browse/DEVOPS-856) Remove labels in cs-ui service
  

- [DEVOPS-845](https://ebsproject.atlassian.net/browse/DEVOPS-845) Create a manifest file
  

- CORB-2661 Add cb and cs urls in cbconfig
  

- DB-1007 Change postgres to ebsadmin user
  

- DB-958 Remove develop label in CB-DB config
  

- DB-974 Add track_activity_query_size param - add: Add track_activity_query_size_param in postgresql.conf
  

- [DEVOPS-905](https://ebsproject.atlassian.net/browse/DEVOPS-905) Update PostgreSQL authentication
  

- Feature/[DEVOPS-826](https://ebsproject.atlassian.net/browse/DEVOPS-826)
  

- Feature/CORB-2935
  




# 22.02.11...22.03.16 (2022-03-01)

- [Fix AF API tag](https://bitbucket.org/ebsproject/deployment-tool/commit/a6d7d165d425384dc8d24ed31e70cb01379531c3)

### Fixes
    
- Update AF API to the latest version

### Uncategorized
    
- Release/21.11.02
  

- Update ba-db env
  

- Fix AF API tag
  


# 22.01.07...22.02.11 (2022-02-11)

- [[DEVOPS-689](https://ebsproject.atlassian.net/browse/DEVOPS-689) Include r402 and r403 as supported parameters for the AF Engine.](https://bitbucket.org/ebsproject/deployment-tool/commit/3fcf77a7c8f2b5afb5e880adfd7a566e4dcd9cdf)
- [Adds support for GDM deployment](https://bitbucket.org/ebsproject/deployment-tool/commit/695357e63f801ab06e41270673a84d3d6c30c917)

### New Features
    
- Include r402 and r403 as valid R versions in the config file. This is required for the R scripts to be correctly executed although internally, there is only one R version in use.

- Add deployment files for GOBii GDM process, db, and timescope

### Uncategorized
    
- [DEVOPS-709](https://ebsproject.atlassian.net/browse/DEVOPS-709) Update variables in cs-ps
  

- [DEVOPS-689](https://ebsproject.atlassian.net/browse/DEVOPS-689) Include r402 and r403 as supported parameters for the AF Engine.
  

- Feature/CORB-2656
  

- SM-567: Adds support for GDM deployment
  

- Feature/[DEVOPS-711](https://ebsproject.atlassian.net/browse/DEVOPS-711)
  


# 21.11.26...22.01.07 (2021-12-07)

- [CORB-2521 Update CB Makefile](https://bitbucket.org/ebsproject/deployment-tool/commit/aad02afdd8b0a025efecbecd485600f56101411b)
- [[DEVOPS-707](https://ebsproject.atlassian.net/browse/DEVOPS-707) Add entry point for CUPS through CS PS.](https://bitbucket.org/ebsproject/deployment-tool/commit/350725a9e47926ba306e849c0c92c3e78681e89d)
- [[DEVOPS-708](https://ebsproject.atlassian.net/browse/DEVOPS-708) Update service definition for CS API.](https://bitbucket.org/ebsproject/deployment-tool/commit/07238fe099ce883243efd9163eef3552e820572f)

### New Features
    
- make target to restore db from fixture

- Configured stack file to allow access to CUPS. This service is running in the CS PS container but accessible using a different URL.

- Update service definition for CS API with the latest variables provided by the CS team.

### Uncategorized
    
- CORB-2521 Update CB Makefile
  

- Add entry point for CUPS in CS PS
  

- CORB-2308 Add CB_DEBUG_ENABLED in cbconfig (default value is FALSE).
  

- [DEVOPS-708](https://ebsproject.atlassian.net/browse/DEVOPS-708) Update service definition for CS API.
  


# 21.11.02...21.11.26 (2021-11-25)

- [Updated Deployment Tool environment for release/21.10.13](https://bitbucket.org/ebsproject/deployment-tool/commit/0ccf5a5edadeec9a04d21c22c31bc6a38b9f5235)
- [CORB-2335 Update Makefile](https://bitbucket.org/ebsproject/deployment-tool/commit/bfe92b45894f8a8770161a2f9f16a41d585bb141)
- [[DEVOPS-640](https://ebsproject.atlassian.net/browse/DEVOPS-640) Remove port ingress for cb-redis in the EBS Deployment Tool](https://bitbucket.org/ebsproject/deployment-tool/commit/6dc6cb2d6eba02c68dbb3566b812afed8d583af3)
- [[DEVOPS-661](https://ebsproject.atlassian.net/browse/DEVOPS-661) Create a guidelines on how to apply SSL certificates in Traefik.](https://bitbucket.org/ebsproject/deployment-tool/commit/d7ee52f39ec95ba7e1fd90b59c8da20263a2c7e5)
- [[DEVOPS-662](https://ebsproject.atlassian.net/browse/DEVOPS-662) Update README.md in the EBS Deployment Tool so that the git clone command is not constrained to a depth of 1.](https://bitbucket.org/ebsproject/deployment-tool/commit/21e23a6567ec6caf7a97c1b9ca9b3958b826ee39)
- [[DEVOPS-648](https://ebsproject.atlassian.net/browse/DEVOPS-648) Update the script in creating backup](https://bitbucket.org/ebsproject/deployment-tool/commit/97b8e6cf03d263627b499a17235c9851d807b3db)
- [[DEVOPS-640](https://ebsproject.atlassian.net/browse/DEVOPS-640) Remove port ingress for cb-redis in the EBS Deployment Tool](https://bitbucket.org/ebsproject/deployment-tool/commit/5c30c2d3c4bb40f23344e2e36f2db72ee61fa025)
- [[DEVOPS-661](https://ebsproject.atlassian.net/browse/DEVOPS-661) Create a guideline on how to apply SSL certificates in Traefik.](https://bitbucket.org/ebsproject/deployment-tool/commit/eb71be2d9fe238c984038cee3d7d004d0e539109)
- [[DEVOPS-662](https://ebsproject.atlassian.net/browse/DEVOPS-662) Update README.md in the EBS Deployment Tool so that the git clone command is not constrained to a depth of 1.](https://bitbucket.org/ebsproject/deployment-tool/commit/6aa6d47df7de0c3651d048b59ca0cf8c3632e8fa)
- [[DEVOPS-641](https://ebsproject.atlassian.net/browse/DEVOPS-641) Update configuration in cb-stack.yml](https://bitbucket.org/ebsproject/deployment-tool/commit/87472195cb136e9db40aab66db40b162776c8083)
- [[DEVOPS-643](https://ebsproject.atlassian.net/browse/DEVOPS-643) Add healthcheck recommendation in BA](https://bitbucket.org/ebsproject/deployment-tool/commit/841dfe4bf003f6f8effa3be1fb5f186ceb227c08)
- [[DEVOPS-644](https://ebsproject.atlassian.net/browse/DEVOPS-644) Add healthcheck recommendation in SM](https://bitbucket.org/ebsproject/deployment-tool/commit/b8e64d4d1da18740276447ac7c09e89437ad48fa)
- [[DEVOPS-645](https://ebsproject.atlassian.net/browse/DEVOPS-645) Add healthcheck recommendation in CS](https://bitbucket.org/ebsproject/deployment-tool/commit/15a27b1d4306eca2a37fbb49c63a3c2bdfc5723a)
- [[DEVOPS-646](https://ebsproject.atlassian.net/browse/DEVOPS-646) Update healthcheck in AF](https://bitbucket.org/ebsproject/deployment-tool/commit/1fefc5d1a47bdc0fefadfe26866029c868d5b800)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Add SM UI in CS](https://bitbucket.org/ebsproject/deployment-tool/commit/68c2ad13e18883f6de2babbdb10d8ff4d56beed9)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Remove arm-ui in CS stack](https://bitbucket.org/ebsproject/deployment-tool/commit/3395f461cccdef75adbfb99e56f68d989ad9bf96)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Add arm-ui in ba-stack.yml](https://bitbucket.org/ebsproject/deployment-tool/commit/9a79d927a1f3878e966e661e3db179443f961d69)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Remove secrets and update hosts](https://bitbucket.org/ebsproject/deployment-tool/commit/6edf8c7dad19a22fb66ea6f515530b028228c684)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Put SM in SM stack](https://bitbucket.org/ebsproject/deployment-tool/commit/8e2d3604f82807295535f8010ac18d2b18804e29)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Update CS env file](https://bitbucket.org/ebsproject/deployment-tool/commit/a4329de8495b1e9d9e49c9f38567428aee537538)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Update BA, CS, and SM env files](https://bitbucket.org/ebsproject/deployment-tool/commit/dcf8bbb628fbb514f1100fd308566ee2636e026c)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Update CS env file](https://bitbucket.org/ebsproject/deployment-tool/commit/c74ae691b13c084051d48e2c963927e7d6e74407)
- [[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649) Update BA and SM env](https://bitbucket.org/ebsproject/deployment-tool/commit/cd70f47e7327216b8d23895e6a72b8c82020b991)
- [[DEVOPS-129](https://ebsproject.atlassian.net/browse/DEVOPS-129) Remove the terraform directory from the EBS Deployment Tool](https://bitbucket.org/ebsproject/deployment-tool/commit/de02fe569d1792ac41f129ba8182bd7594612760)

### New Features
    
- Created a script that updates the env file of specific domain/s reflecting new Docker image tags

- Added CLIENT_ID and CLIENT_SECRET in the environment variables of CB Workers

- Created a script to generate CHANGELOG.md in EBS Deployment Tool

- Added Makefile in CB

- Updated the env variables for Breeding Analytics to reflect ASReml-related changes

- Updated the env variables for Core System to support user synchronization between the Core Breeding and Core System databases

- Added support through an env variable for increasing the timeout for randomization requests in the Analytics Framework

- Added a script under utility that will create new connections in Vault.

- Added CB UI test targets

- Separated targets for package management in CB UI, API, Workers

- Updated the README.md in coruscant to add guidelines on how to apply SSL certificates in Traefik.

- Updated the git clone commands in the EBS Deployment Tool.

- Added confirmation prompt prior to actually creating the backup. Included the full path of the location of the backup file.

- Updated the README.md in coruscant to add a guideline on how to apply SSL certificates in Traefik.

- Updated the git clone commands in the EBS Deployment Tool.

- Updated configuration in cb-stack.yml to support zero downtime deployment

- Added healthcheck recommendation in ba-api and ba-arm services.

- Added healthcheck recommendation in sm-api and sm-ui services.

- Added healthcheck recommendation in cs-api, cs-ebs-components, cs-ebs-shared-dependencies, cs-ebs-styleguide, cs-ebs-ui-proxy, cs-ps, and cs-ui services.

- Updated healthcheck in ebs-af-postgres, ebs-af-rafiki, and ebs-sg-af services.

- add SM Docker variables in CS env file

- add sm-ui in cs-stack.yml

- arm-ui in ba-stack.yml

- ARM variables in BA env file

- dev hosts in CS and BA env files

- CS_ASSETS_HOST in SM env file

- dev tag in CS_UI_IMAGE_TAG

### Uncategorized
    
- Release/21.10.13
  

- CORB-2335 Update Makefile
  

- [DEVOPS-648](https://ebsproject.atlassian.net/browse/DEVOPS-648) Create a script to backup the EBS Deployment Tool directory of a cluster
  

- [DEVOPS-640](https://ebsproject.atlassian.net/browse/DEVOPS-640) Remove port ingress for cb-redis in the EBS Deployment Tool
  

- Feature/[DEVOPS-661](https://ebsproject.atlassian.net/browse/DEVOPS-661)
  

- Feature/[DEVOPS-650](https://ebsproject.atlassian.net/browse/DEVOPS-650)
  

- [DEVOPS-641](https://ebsproject.atlassian.net/browse/DEVOPS-641) Update configuration in cb-stack.yml
  

- [DEVOPS-641](https://ebsproject.atlassian.net/browse/DEVOPS-641) Add healthcheck in cb-redis; Add cb-redis as 'depends_on' of cb;
  

- [DEVOPS-643](https://ebsproject.atlassian.net/browse/DEVOPS-643) Add healthcheck recommendation in BA
  

- [DEVOPS-644](https://ebsproject.atlassian.net/browse/DEVOPS-644) Add healthcheck recommendation in SM
  

- [DEVOPS-645](https://ebsproject.atlassian.net/browse/DEVOPS-645) Add healthcheck recommendation in CS
  

- [DEVOPS-646](https://ebsproject.atlassian.net/browse/DEVOPS-646) Update healthcheck in AF
  

- Feature/[DEVOPS-649](https://ebsproject.atlassian.net/browse/DEVOPS-649)
  

- [DEVOPS-129](https://ebsproject.atlassian.net/browse/DEVOPS-129) Remove the terraform directory from the EBS Deployment Tool
  

### Deletions
    
- Removed port ingress for cb-redis in the EBS Deployment Tool

- Removed port ingress for cb-redis in the EBS Deployment Tool

- arm-ui in cs-stack.yml

- ARM variables in CS env file

- secrets in CS env file

- SM variables in CS env file

- sm-ui in cs-stack.yml

- specific URLs in host variables

- specific URLs in host variables

- secrets in env files

- specific URLs of hosts in env files

- Removed the terraform directory from the EBS Deployment Tool


# 21.10.13...21.11.02 (2021-11-02)

- [Updated Deployment Tool environment for release/21.10.13](https://bitbucket.org/ebsproject/deployment-tool/commit/0ccf5a5edadeec9a04d21c22c31bc6a38b9f5235)
- [[DEVOPS-625](https://ebsproject.atlassian.net/browse/DEVOPS-625) Update the CS API service in the EBS Deployment Tool to include new environment variables](https://bitbucket.org/ebsproject/deployment-tool/commit/2fe2f8e76698bf4a4ea2fa39fd9af2371a50b37c)
- [Revert "CORB-2069 Move Makefile to core_breeding"](https://bitbucket.org/ebsproject/deployment-tool/commit/24f6d392093c513f481fed4bcb91b000ff018f2f)
- [CORB-2069 Move Makefile to core_breeding](https://bitbucket.org/ebsproject/deployment-tool/commit/4906d62abd2d567eff2c46abf08159256dd4dd2f)
- [[DEVOPS-624](https://ebsproject.atlassian.net/browse/DEVOPS-624) Replace configuration with new default credentials for RabbitMQ.](https://bitbucket.org/ebsproject/deployment-tool/commit/e285f150e6549d7c1dd810599efa0a7789e40335)
- [[DEVOPS-624](https://ebsproject.atlassian.net/browse/DEVOPS-624) Replace configuration with new default credentials for RabbitMQ.](https://bitbucket.org/ebsproject/deployment-tool/commit/8b2e1ec86310fc7aa308ef73351aeb184a8ab09b)
- [[DEVOPS-625](https://ebsproject.atlassian.net/browse/DEVOPS-625) Update the CS API service in the EBS Deployment Tool to include new environment variables](https://bitbucket.org/ebsproject/deployment-tool/commit/7dd571bec2d825254e788413bd3b23dbab0af095)

### Security Updates
    
- Replace default password for RabbitMQ

- Replace default password for RabbitMQ

### New Features
    
- Created a script that updates the env file of specific domain/s reflecting new Docker image tags

- Added CLIENT_ID and CLIENT_SECRET in the environment variables of CB Workers

- Created a script to generate CHANGELOG.md in EBS Deployment Tool

- Added Makefile in CB

- Updated the env variables for Breeding Analytics to reflect ASReml-related changes

- Updated the env variables for Core System to support user synchronization between the Core Breeding and Core System databases

- Added support through an env variable for increasing the timeout for randomization requests in the Analytics Framework

- Added a script under utility that will create new connections in Vault.

- Added wso2_cs_serverData in the environment variables of CS API service

- Added wso2_cs_serverData in the environment variables of CS API service

### Uncategorized
    
- Replace configuration with new default credentials for RabbitMQ.
  

- [DEVOPS-625](https://ebsproject.atlassian.net/browse/DEVOPS-625) Update the CS API service in the EBS Deployment Tool to include new environment variables
  

- This reverts commit 5b62d3c66a278afd99226433f7d1dda4f049ce7a.

- Accidentally included other files when updating commit message in origin


# 21.05...21.10.13 (2021-10-13)

- [Revert "[DEVOPS-429](https://ebsproject.atlassian.net/browse/DEVOPS-429) Add the WSO2 environment variables for the CS API"](https://bitbucket.org/ebsproject/deployment-tool/commit/6e18fa892971c18511aafe0e90f0f6c7cae7adad)
- [Revert "CORB-2069 Move Makefile to core_breeding"](https://bitbucket.org/ebsproject/deployment-tool/commit/49857fe6e97a2e9d2a1acf6a9e550b96961daf58)
- [CORB-2069 Move Makefile to core_breeding](https://bitbucket.org/ebsproject/deployment-tool/commit/dd107c03e7e2cae4b652e47970513049850941c1)
- [Updated Deployment Tool environment for release/21.10.13](https://bitbucket.org/ebsproject/deployment-tool/commit/0ccf5a5edadeec9a04d21c22c31bc6a38b9f5235)

### New Features
    
- Created a script that updates the env file of specific domain/s reflecting new Docker image tags

- Added CLIENT_ID and CLIENT_SECRET in the environment variables of CB Workers

- Created a script to generate CHANGELOG.md in EBS Deployment Tool

- Added Makefile in CB

- Updated the env variables for Breeding Analytics to reflect ASReml-related changes

- Updated the env variables for Core System to support user synchronization between the Core Breeding and Core System databases

- Added support through an env variable for increasing the timeout for randomization requests in the Analytics Framework

- Added a script under utility that will create new connections in Vault.

### Uncategorized
    
- [DEVOPS-333](https://ebsproject.atlassian.net/browse/DEVOPS-333) Update Docker Images
  

- Fix BA database name in AF stack
  

- [DEVOPS-358](https://ebsproject.atlassian.net/browse/DEVOPS-358) Add error-page service in the coruscant stack file; Update core breeding stack file;
  

- [DEVOPS-358](https://ebsproject.atlassian.net/browse/DEVOPS-358) Fix labels config
  

- [DEVOPS-359](https://ebsproject.atlassian.net/browse/DEVOPS-359) Modify stack files to display the custom error page when Traefik receives an error
  

- Add missing $ in variable.
  

- Feature/[DEVOPS-359](https://ebsproject.atlassian.net/browse/DEVOPS-359)
  

- Feature/[DEVOPS-373](https://ebsproject.atlassian.net/browse/DEVOPS-373)
  

- Added healthcheck in RabbitMQ service
  

- Feature/[DEVOPS-330](https://ebsproject.atlassian.net/browse/DEVOPS-330)
  

- [DEVOPS-330](https://ebsproject.atlassian.net/browse/DEVOPS-330) Change CS_PS_HOST variable name
  

- Add CS_PS_URL parameter in cbconfig.
  

- Update volume mount for AF API.
  

- Feature/[DEVOPS-398](https://ebsproject.atlassian.net/browse/DEVOPS-398)
  

- [DEVOPS-397](https://ebsproject.atlassian.net/browse/DEVOPS-397) Update BA stack files
  

- [DEVOPS-397](https://ebsproject.atlassian.net/browse/DEVOPS-397) Fix port in BA API
  

- ESD-596 Add cornell.edu to fix user creation error
  

- ESD-791 Limit the database connection created by BSO-API
  

- Change permission for analytics/output directory so CB can write on it
  

- [DEVOPS-385](https://ebsproject.atlassian.net/browse/DEVOPS-385) Create script that will automatically update the FQDNs for each domain
  

- Replace CB_RABBITMQ_HOST with RABBITMQ_HOST.
  

- This reverts commit b0fe260aa99b301dbdd8198b8df77b9a66b8a866.

- [DEVOPS-429](https://ebsproject.atlassian.net/browse/DEVOPS-429) Add the WSO2 environment variables for the CS API
  

- [DEVOPS-443](https://ebsproject.atlassian.net/browse/DEVOPS-443) Add the docker stack name as a variable in the labels of CB stack
  

- [DEVOPS-466](https://ebsproject.atlassian.net/browse/DEVOPS-466) Add the docker stack name as environment variable for BA
  

- Implement script to quickly remove all services of a component.
  

- [DEVOPS-468](https://ebsproject.atlassian.net/browse/DEVOPS-468) Add EBS docker stack name to the labels as variable for Service Management
  

- [DEVOPS-467](https://ebsproject.atlassian.net/browse/DEVOPS-467) Add the EBS docker stack name to the labels of BSO API
  

- Update configuration for the BA stack
  

- [DEVOPS-465](https://ebsproject.atlassian.net/browse/DEVOPS-465) Add docker stack label to the labels of cs-api, cs-ps, and cs-ui
  

- [DEVOPS-507](https://ebsproject.atlassian.net/browse/DEVOPS-507) Update cb-db service
  

- Feature/[DEVOPS-475](https://ebsproject.atlassian.net/browse/DEVOPS-475)
  

- [DEVOPS-510](https://ebsproject.atlassian.net/browse/DEVOPS-510) Update rabbitmq service in coruscant stack file
  

- Feature/[DEVOPS-520](https://ebsproject.atlassian.net/browse/DEVOPS-520)
  

- Feature/[DEVOPS-496](https://ebsproject.atlassian.net/browse/DEVOPS-496)
  

- [DEVOPS-498](https://ebsproject.atlassian.net/browse/DEVOPS-498) Update stack files in service management to enable retrieving database credentials from vault
  

- Feature/[DEVOPS-499](https://ebsproject.atlassian.net/browse/DEVOPS-499)
  

- Feature/[DEVOPS-497](https://ebsproject.atlassian.net/browse/DEVOPS-497)
  

- [DEVOPS-527](https://ebsproject.atlassian.net/browse/DEVOPS-527) Changed incorrect labels in the Core System stack file
  

- Integrate CS EBS Style Guide Service
  

- Integrate CS EBS Components Service
  

- Integrate CS EBS Shared Components Service
  

- [DEVOPS-575](https://ebsproject.atlassian.net/browse/DEVOPS-575) Add _ prefix in labels of BA and SM stack file
  

- Update ENV variables for CS to allow for user credentials synchronization
  

- Replace env variables for BA Worker
  

- Include parameter for FPO expiry time
  

- [DEVOPS-522](https://ebsproject.atlassian.net/browse/DEVOPS-522) Create a script to create connection in Vault
  

- [DEVOPS-592](https://ebsproject.atlassian.net/browse/DEVOPS-592) Add CLIENT_ID and CLIENT_SECRET in cb-workers
  

- Feature/[DEVOPS-207](https://ebsproject.atlassian.net/browse/DEVOPS-207)
  

- [DEVOPS-618](https://ebsproject.atlassian.net/browse/DEVOPS-618) Create a script to generate changelog
  

- This reverts commit 5b62d3c66a278afd99226433f7d1dda4f049ce7a.

- Accidentally included other files when updating commit message in origin

- Add Makefile to support local development
  


# 21.04.02...21.05 (2021-06-01)


### Uncategorized
    
- [DEVOPS-306](https://ebsproject.atlassian.net/browse/DEVOPS-306) Utilize the BA DB for the AF API (randomization)
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Update config files; Remove temporary variables;
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Update API URL variables in SM
  

- [DEVOPS-319](https://ebsproject.atlassian.net/browse/DEVOPS-319) Create script to download all Docker images
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Add EBS logo; Update CB stack;
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Add environment variable in BA stack
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Update variable name in BA stack
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Update BA stack file
  

- [DEVOPS-323](https://ebsproject.atlassian.net/browse/DEVOPS-323) Add environment variable in CS stack
  


# 21.04...21.04.02 (2021-05-13)


### Uncategorized
    
- [DEVOPS-270](https://ebsproject.atlassian.net/browse/DEVOPS-270) Update configuration of AFDB
  

- [DEVOPS-270](https://ebsproject.atlassian.net/browse/DEVOPS-270) Remove unused variables
  

- [DEVOPS-308](https://ebsproject.atlassian.net/browse/DEVOPS-308) Remove unused files related to CB
  

- Feature/[DEVOPS-275](https://ebsproject.atlassian.net/browse/DEVOPS-275)
  

- [DEVOPS-276](https://ebsproject.atlassian.net/browse/DEVOPS-276) Update cs-db service to use Dockerized Core System DB
  

- Feature/[DEVOPS-266](https://ebsproject.atlassian.net/browse/DEVOPS-266)
  

- [DEVOPS-266](https://ebsproject.atlassian.net/browse/DEVOPS-266) Remove b4r references in README.md files
  

- Feature/[DEVOPS-277](https://ebsproject.atlassian.net/browse/DEVOPS-277)
  


# 21.03...21.04 (2021-05-04)


### Uncategorized
    
- [DEVOPS-234](https://ebsproject.atlassian.net/browse/DEVOPS-234) Update instrunctions in README.md for using util-run.sh
  

- [DEVOPS-238](https://ebsproject.atlassian.net/browse/DEVOPS-238) Include breeding_station_operations in components in README.md
  

- [DEVOPS-209](https://ebsproject.atlassian.net/browse/DEVOPS-209) Add configuration file
  

- [DEVOPS-210](https://ebsproject.atlassian.net/browse/DEVOPS-210) Use variables for hosts, stack name and network name for the Core Breeding stack
  

- [DEVOPS-212](https://ebsproject.atlassian.net/browse/DEVOPS-212) Use a variable for the stack name and the network name for the Breeding Station Operations stack.
  

- [DEVOPS-211](https://ebsproject.atlassian.net/browse/DEVOPS-211) Use a variable for the stack name and the network name for the Analytics Framework stack.
  

- Feature/[DEVOPS-210](https://ebsproject.atlassian.net/browse/DEVOPS-210)
  

- [DEVOPS-123](https://ebsproject.atlassian.net/browse/DEVOPS-123) Deploy Traefik as a separate stack
  

- Consolidate Pulling of Docker Images Into One Script
  

- [DEVOPS-222](https://ebsproject.atlassian.net/browse/DEVOPS-222) Separate RabbitMQ from core breeding
  

- [DEVOPS-222](https://ebsproject.atlassian.net/browse/DEVOPS-222) Remove parameters for RabbitMQ
  

- Removed unused variable
  

- Updated RabbitMQ host in b4rconfig and b4rapiconfig
  

- [DEVOPS-224](https://ebsproject.atlassian.net/browse/DEVOPS-224) Update BSO API service
  

- [DEVOPS-265](https://ebsproject.atlassian.net/browse/DEVOPS-265) UIpdate incorrect port for BSO API in Core Breeding Configuration
  

- [DEVOPS-246](https://ebsproject.atlassian.net/browse/DEVOPS-246)  Update config_stack.sh in AF to create necessary directories
  

- Integration of Service Management to the EBS Deployment Tool
  

- [DEVOPS-257](https://ebsproject.atlassian.net/browse/DEVOPS-257) Update Core Breeding component to use the pre-populated Core Breeding DB service
  

- [DEVOPS-245](https://ebsproject.atlassian.net/browse/DEVOPS-245) Format shell scripts
  

- Fix sm-db environment variables
  

- Rebranding of Tenant to CS
  

- Replace environment variables using the stack file
  

- Feature/SM-82 merge
  

- Integration of Breeding Analytics to the EBS Deployment Tool
  

- [DEVOPS-249](https://ebsproject.atlassian.net/browse/DEVOPS-249) Add AFDB env variable.
  


